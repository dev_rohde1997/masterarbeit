-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema database
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema database
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `database` ;
USE `database` ;

-- -----------------------------------------------------
-- Table `database`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `database`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `database`.`learning_style`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `database`.`learning_style` (
  `user_id` INT NOT NULL,
  `sensing` INT NULL,
  `intuitive` INT NULL,
  `visual` INT NULL,
  `verbal` INT NULL,
  `active` INT NULL,
  `reflective` INT NULL,
  `sequential` INT NULL,
  `global` INT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_ILS_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `database`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `database`.`lecture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `database`.`lecture` (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `database`.`user_took_lecture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `database`.`user_took_lecture` (
  `user_id` INT NOT NULL,
  `lecture_id` INT NOT NULL,
  `rating` INT NOT NULL,
  `time` DATETIME NOT NULL,
  PRIMARY KEY (`user_id`, `lecture_id`),
  INDEX `fk_user_took_lecture_lecture_id_idx` (`lecture_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_took_lecture_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `database`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_took_lecture_lecture_id`
    FOREIGN KEY (`lecture_id`)
    REFERENCES `database`.`lecture` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `database`.`result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `database`.`result` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `correct` INT NULL,
  `wrong` INT NULL,
  `time` DATETIME NULL,
  `user_id` INT NOT NULL,
  `lecture_id` INT NOT NULL,
  `warning` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_result_user_id_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_result_lecture_id_idx` (`lecture_id` ASC) VISIBLE,
  CONSTRAINT `fk_result_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `database`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_result_lecture_id`
    FOREIGN KEY (`lecture_id`)
    REFERENCES `database`.`lecture` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE USER 'user' IDENTIFIED BY 'user';

GRANT ALL ON `database`.* TO 'user';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `database`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `database`;
INSERT INTO `database`.`user` (`id`, `name`) VALUES (1, 'Moritz');
INSERT INTO `database`.`user` (`id`, `name`) VALUES (2, 'Lina');

COMMIT;


-- -----------------------------------------------------
-- Data for table `database`.`learning_style`
-- -----------------------------------------------------
START TRANSACTION;
USE `database`;
INSERT INTO `database`.`learning_style` (`user_id`, `sensing`, `intuitive`, `visual`, `verbal`, `active`, `reflective`, `sequential`, `global`) VALUES (2, 8, 3, 1, 10, 10, 1, 9, 2);
INSERT INTO `database`.`learning_style` (`user_id`, `sensing`, `intuitive`, `visual`, `verbal`, `active`, `reflective`, `sequential`, `global`) VALUES (1, 3, 8, 10, 1, 1, 10, 2, 9);

COMMIT;


-- -----------------------------------------------------
-- Data for table `database`.`lecture`
-- -----------------------------------------------------
START TRANSACTION;
USE `database`;
INSERT INTO `database`.`lecture` (`id`, `name`) VALUES (1, 'Erweitern und Kürzen');
INSERT INTO `database`.`lecture` (`id`, `name`) VALUES (2, 'Größenvergleich');
INSERT INTO `database`.`lecture` (`id`, `name`) VALUES (3, 'Addition und Subtraktion');
INSERT INTO `database`.`lecture` (`id`, `name`) VALUES (4, 'Multiplikation und Division');

COMMIT;


-- -----------------------------------------------------
-- Data for table `database`.`user_took_lecture`
-- -----------------------------------------------------
START TRANSACTION;
USE `database`;
INSERT INTO `database`.`user_took_lecture` (`user_id`, `lecture_id`, `rating`, `time`) VALUES (1, 1, 0, '2022-02-11 12:00:00.000');
INSERT INTO `database`.`user_took_lecture` (`user_id`, `lecture_id`, `rating`, `time`) VALUES (1, 2, 0, '2022-02-11 12:00:00.000');
INSERT INTO `database`.`user_took_lecture` (`user_id`, `lecture_id`, `rating`, `time`) VALUES (1, 3, 0, '2022-02-11 12:00:00.000');
INSERT INTO `database`.`user_took_lecture` (`user_id`, `lecture_id`, `rating`, `time`) VALUES (1, 4, 0, '2022-02-11 12:00:00.000');


COMMIT;


-- -----------------------------------------------------
-- Data for table `database`.`user_took_lecture`
-- -----------------------------------------------------
START TRANSACTION;
USE `database`;
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (1, 1, 1, 7, 1, 2, '2022-03-01 12:00:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (2, 1, 1, 7, 1, 2, '2022-03-01 12:10:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (3, 2, 1, 7, 1, 2, '2022-03-01 12:20:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (4, 2, 1, 7, 1, 2, '2022-03-01 12:30:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (5, 3, 1, 7, 1, 2, '2022-03-01 12:40:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (6, 3, 1, 7, 1, 2, '2022-03-01 12:50:00.000');
INSERT INTO `database`.`result` (`id`, `lecture_id`, `user_id`, `correct`, `wrong`, `warning`, `time`) VALUES (7, 4, 1, 7, 1, 2, '2022-03-01 13:00:00.000');


COMMIT;

