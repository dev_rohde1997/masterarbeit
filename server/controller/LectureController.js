// Imports
const httpStatus = require('http-status');
const moment = require('moment');
const LearningStyleModel = require('../models/learning_style');

// Import models
const LectureModel = require('../models/lecture');
const UserModel = require('../models/user');
const UserTookLectureModel = require('../models/userTookLecture');
const TaskGenerator = require('../tasks/TaskGenerator');
// const LearningStyle = require('../models/learning_style');

const LOCK_LEVEL_VALUE = 50;
const DOMINANT_LEARNING_STYLE_THRESHOLD = 6;

/**
 * Controller class for user table
 * @see {@link User}
 */
class LectureController {
  async getAll(req, res) {
    try {
      const { userId } = req.params;

      // Check user
      const user = await UserModel.getById(userId);
      if (user.length === 0) {
        return res.status(httpStatus.NOT_FOUND).json();
      }

      // Get learning style
      const learningStlye = await LearningStyleModel.getById(userId);
      let isGlobalLearner = false;
      if (learningStlye.length !== 0) {
        isGlobalLearner =
          learningStlye[0].global > DOMINANT_LEARNING_STYLE_THRESHOLD;
      }

      // Get lecture
      let lectures = await LectureModel.getAllByUserId(userId);
      if (lectures.length === 0) {
        // Create lecture connection
        const allLectures = await LectureModel.getAll();
        for (const lecture of allLectures) {
          await UserTookLectureModel.add(
            new UserTookLectureModel({
              user_id: userId,
              lecture_id: lecture.id,
              rating: 0,
              time: moment().format('YYYY-MM-DD HH:mm:ss.000'),
            })
          );
        }
        lectures = await LectureModel.getAllByUserId(userId);
      }

      let response = [];
      let lastRating = 100;
      for (const lecture of lectures) {
        // Check if lecture is locked
        let locked = lastRating < LOCK_LEVEL_VALUE && !isGlobalLearner;
        lastRating = lecture.rating;
        response.push({
          userName: lecture.userName,
          userId: lecture.userId,
          lectureName: lecture.lectureName,
          lectureId: lecture.lectureId,
          rating: lecture.rating,
          locked,
        });
      }

      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  async getTask(req, res) {
    try {
      const { userId, lectureId } = req.params;

      // Check user
      const user = await UserModel.getById(userId);
      if (user.length === 0) {
        return res.status(httpStatus.NOT_FOUND).json();
      }

      // Check lecture
      const lecture = await LectureModel.getById(lectureId);
      if (lecture.length === 0) {
        return res.status(httpStatus.NOT_FOUND).json();
      }

      // Get learning style
      const learningStyleEntry = await LearningStyleModel.getById(userId);
      let learningStyle = null;
      if (learningStyleEntry.length !== 0) {
        const { visual } = learningStyleEntry[0];
        const { verbal } = learningStyleEntry[0];

        if (visual > verbal) {
          learningStyle = 'visual';
        } else {
          learningStyle = 'verbal';
        }
      }

      // Get skills level
      const skillEntry = await UserTookLectureModel.getById(userId, lectureId);
      const skillLevel = skillEntry[0].rating;

      let tasks = [];

      switch (parseInt(lectureId, 10)) {
        case 1:
          // Get tasks for shorten and extension
          tasks.push(...LectureController.getExtensionTask(skillLevel));
          tasks.push(...LectureController.getShortenTask(skillLevel));
          break;
        case 2:
          tasks.push(...LectureController.getSizeComparisonTask(skillLevel));
          tasks.push(...LectureController.getOrderSize(skillLevel));
          // }
          break;
        case 3:
          tasks.push(...LectureController.getAdditionTask(skillLevel));
          tasks.push(...LectureController.getSubtractionTask(skillLevel));
          break;
        case 4:
          tasks.push(...LectureController.getMultiplyTask(skillLevel));
          tasks.push(...LectureController.getDivisionTask(skillLevel));
          break;
        default:
          break;
      }

      const response = {
        name: lecture[0].name,
        tasks,
        learningStyle,
      };
      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  static getShortenTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get simple task
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleShortenTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateShortenTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardShortenTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getExtensionTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleExtensionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateExtensionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardExtensionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getSizeComparisonTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleSizeComparisonTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateSizeComparisonTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardSizeComparisonTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getOrderSize(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleOrderSizeTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateOrderSizeTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardOrderSizeTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getAdditionTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleAdditionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            (mTask.denumerator === newTask.denumerator &&
              mTask.numerator === newTask.numerator &&
              mTask.denumerator2 === newTask.denumerator2 &&
              mTask.numerator2 === newTask.numerator2 &&
              mTask.description === newTask.description) ||
            (mTask.denumerator2 === newTask.denumerator &&
              mTask.numerator2 === newTask.numerator &&
              mTask.denumerator === newTask.denumerator2 &&
              mTask.numerator === newTask.numerator2 &&
              mTask.description === newTask.description)
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateAdditionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            (mTask.denumerator === newTask.denumerator &&
              mTask.numerator === newTask.numerator &&
              mTask.denumerator2 === newTask.denumerator2 &&
              mTask.numerator2 === newTask.numerator2 &&
              mTask.description === newTask.description) ||
            (mTask.denumerator2 === newTask.denumerator &&
              mTask.numerator2 === newTask.numerator &&
              mTask.denumerator === newTask.denumerator2 &&
              mTask.numerator === newTask.numerator2 &&
              mTask.description === newTask.description)
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardAdditionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            (mTask.denumerator === newTask.denumerator &&
              mTask.numerator === newTask.numerator &&
              mTask.denumerator2 === newTask.denumerator2 &&
              mTask.numerator2 === newTask.numerator2 &&
              mTask.description === newTask.description) ||
            (mTask.denumerator2 === newTask.denumerator &&
              mTask.numerator2 === newTask.numerator &&
              mTask.denumerator === newTask.denumerator2 &&
              mTask.numerator === newTask.numerator2 &&
              mTask.description === newTask.description)
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getSubtractionTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleSubtractionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateSubtractionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardSubtractionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getMultiplyTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleMultiplyTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateMultiplyTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardMultiplyTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  static getDivisionTask(skillLevel) {
    let tasks = [];
    if (skillLevel < 33) {
      // Get extension tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getSimpleDivisionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else if (skillLevel < 66) {
      // Get intermediate tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getIntermediateDivisionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    } else {
      // Get hard tasks
      a: for (let i = 0; i < 5; i += 1) {
        const newTask = TaskGenerator.getHardDivisionTask();
        // Check if tasks contains
        for (const mTask of tasks) {
          if (
            mTask.denumerator === newTask.denumerator &&
            mTask.numerator === newTask.numerator &&
            mTask.denumerator2 === newTask.denumerator2 &&
            mTask.numerator2 === newTask.numerator2 &&
            mTask.description === newTask.description
          ) {
            i -= 1;
            continue a;
          }
        }
        tasks.push(newTask);
      }
    }
    return tasks;
  }

  /**
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async add(req, res) {
    try {
      const { name } = req.body;

      console.log(name);
      return res.status(httpStatus.CREATED).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }
}

module.exports = new LectureController();
