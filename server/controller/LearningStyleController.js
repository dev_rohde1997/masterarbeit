// Imports
const httpStatus = require('http-status');
const LearningStyle = require('../models/learning_style');

// Import models
const LearningStyleModel = require('../models/learning_style');

/**
 * Controller class for user table
 * @see {@link User}
 */
class LearningStyleController {
  async get(req, res) {
    try {
      const { id } = req.params;

      const learningStyle = await LearningStyleModel.getById(id);

      let response = {};
      if (learningStyle.length !== 0) {
        response.userId = learningStyle[0].user_id;
        response.data = [
          { Faktisch: learningStyle[0].sensing },
          { Intuitiv: learningStyle[0].intuitive },
          { Visuell: learningStyle[0].visual },
          { Verbal: learningStyle[0].verbal },
          { Aktiv: learningStyle[0].active },
          { Reflektierend: learningStyle[0].reflective },
          { Sequentiell: learningStyle[0].sequential },
          { Global: learningStyle[0].global },
        ];
      }

      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(error());
    }
  }

  /**
   * https://nanopdf.com/download/felder-amp-solomon-8-l-university-of-bradford_pdf
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async add(req, res) {
    try {
      const { userId, data } = req.body;

      // Slice array
      const testData = data.slice(1, 45);

      // Counter for styles
      let sensing = 0;
      let intuitive = 0;
      let verbal = 0;
      let visual = 0;
      let active = 0;
      let reflective = 0;
      let global = 0;
      let sequential = 0;

      // Evaluate test data
      // eslint-disable-next-line guard-for-in
      for (let counter in testData) {
        switch (counter % 4) {
          case 0:
            if (testData[counter] === 1) {
              active += 1;
            } else {
              reflective += 1;
            }
            break;
          case 1:
            if (testData[counter] === 1) {
              sensing += 1;
            } else {
              intuitive += 1;
            }
            break;
          case 2:
            if (testData[counter] === 1) {
              visual += 1;
            } else {
              verbal += 1;
            }
            break;
          case 3:
            if (testData[counter] === 1) {
              sequential += 1;
            } else {
              global += 1;
            }
            break;
          default:
        }
      }

      // Get data
      const learningStyle = await LearningStyle.getById(userId);

      if (learningStyle.length !== 0) {
        // Update
        await LearningStyle.update(
          new LearningStyle({
            user_id: userId,
            sensing,
            intuitive,
            active,
            reflective,
            verbal,
            visual,
            global,
            sequential,
          }),
          userId
        );
      } else {
        // Add
        await LearningStyle.add(
          new LearningStyle({
            user_id: userId,
            sensing,
            intuitive,
            active,
            reflective,
            verbal,
            visual,
            global,
            sequential,
          })
        );
      }
      // Add data to db

      return res.status(httpStatus.CREATED).json();
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(error());
    }
  }
}

module.exports = new LearningStyleController();
