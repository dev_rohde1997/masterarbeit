// Imports
const httpStatus = require('http-status');
const moment = require('moment');
// Import models
const ResultModel = require('../models/result');
const UserTookLecture = require('../models/userTookLecture');

/**
 * Controller class for user table
 * @see {@link User}
 */
class ResultController {
  async get(req, res) {
    try {
      // User id
      const { id } = req.params;

      const result = await ResultModel.getByUserId(id);

      const response = [];
      for (const r of result) {
        const mTime = new Date(r.time).toISOString();
        const mDate = mTime.substring(0, 10).split('-');
        const date = `${mDate[2]}.${mDate[1]}.${mDate[0]}`;
        const time = mTime.substring(11, 16);
        response.push({
          name: r.name,
          correct: r.correct,
          wrong: r.wrong,
          warning: r.warning,
          time,
          date,
        });
      }

      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  /**
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async add(req, res) {
    try {
      // eslint-disable-next-line object-curly-newline
      const { lectureId, correctCount, wrongCount, warningCount } = req.body;
      const { id } = req.params;
      const time = moment().format('YYYY-MM-DD HH:mm:ss.000');
      const resultJson = {
        lecture_id: lectureId,
        wrong: wrongCount,
        correct: correctCount,
        warning: warningCount,
        time,
        user_id: id,
      };
      const newResult = await ResultModel.add(new ResultModel(resultJson));

      // Get created user
      const result = await ResultModel.getById(newResult.insertId);

      // Get old rating
      const oldLectureRating = await UserTookLecture.getById(id, lectureId);

      // Calculate new rating
      const learningFactor = 3;
      const newPlusRating =
        (correctCount + warningCount * 0.5) * learningFactor;
      const newMinusRating = wrongCount * learningFactor;

      let oldRating = 0;
      if (oldLectureRating.length === 0) {
        // No rating yet
        let rating = newPlusRating - newMinusRating;

        // Next level if zero mistakes
        if (wrongCount === 0) {
          rating = 35;
        }

        if (rating < 0) rating = 0;
        if (rating > 100) rating = 100;

        await UserTookLecture.add(
          new UserTookLecture({
            user_id: id,
            lecture_id: lectureId,
            rating,
            time,
          })
        );
      } else {
        oldRating = oldLectureRating[oldLectureRating.length - 1].rating;
        let rating = oldRating + newPlusRating - newMinusRating;

        // Upgrade level
        if (wrongCount === 0) {
          // Next level
          if (oldRating < 33 && rating < 33) {
            rating = 35;
          } else if (oldRating < 66 && rating < 66) {
            rating = 65;
          }
        }

        // Downgrade level
        if (wrongCount >= 8) {
          // Next level
          if (oldRating > 66 && rating > 66) {
            rating = 60;
          } else if (oldRating > 33 && rating > 33) {
            rating = 30;
          }
        }

        if (rating < 0) rating = 0;
        if (rating > 100) rating = 100;

        await UserTookLecture.updateRating(rating, id, lectureId);
      }

      return res.status(httpStatus.CREATED).json(result);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }
}

module.exports = new ResultController();
