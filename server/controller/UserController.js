// Imports
const httpStatus = require('http-status');

// Import models
const UserModel = require('../models/user');

/**
 * Controller class for user table
 * @see {@link User}
 */
class UserController {
  async get(req, res) {
    try {
      const { id } = req.params;

      const user = await UserModel.getById(id);

      let response = {};
      if (user.length !== 0) {
        response.userId = user[0].id;
        response.name = user[0].name;
      }

      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  /**
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async add(req, res) {
    try {
      const { name } = req.body;

      const newUser = await UserModel.add(new UserModel({ name }));

      // Get created user
      const user = await UserModel.getById(newUser.insertId);

      let response = {};
      if (user.length !== 0) {
        response.userId = user[0].id;
        response.name = user[0].name;
      }

      return res.status(httpStatus.CREATED).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  async login(req, res) {
    try {
      const { name } = req.body;

      let user = await UserModel.getByName(name);
      let response = {};
      if (user.length === 0) {
        return res.status(httpStatus.NOT_FOUND).json();
      }
      response.userId = user[0].id;
      response.name = user[0].name;
      return res.status(httpStatus.OK).json(response);
    } catch (err) {
      console.log(err);
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }
}

module.exports = new UserController();
