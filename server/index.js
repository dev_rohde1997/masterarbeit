// Imports
const express = require('express');
const path = require("path");

const app = express();
const morgan = require('morgan');
let cors = require('cors');

let allowedOrigins = ['http://localhost:3000'];

// Database
const db = require('./util/DBConnector');

// Start SQL Connection
db.connect();

// Define PORT that will listen for HTTP requests
const PORT = process.env.PORT || 80;

// default middleware
app.use(express.json({ extended: false }));

app.use(express.static("public"));

app.use(cors());
app.use(morgan('tiny'));

// Bringing routes
const learningStyle = require('./routes/api/v1/learning_style');
const user = require('./routes/api/v1/user');
const lecture = require('./routes/api/v1/lecture');
const result = require('./routes/api/v1/result');

// Using Routers
app.use('/api/v1/learning-style', learningStyle);
app.use('/api/v1/users', user);
app.use('/api/v1/lectures', lecture);
app.use('/api/v1/results', result);

// Serve react SPA
app.use((req, res, next) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

// Server Listening at PORT process.env.PORT || 80
app.listen(PORT, () => {
  console.log(`Server starting at port -> ${PORT}`);
});

module.exports = app;
