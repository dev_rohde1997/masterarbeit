const faker = require('@faker-js/faker/locale/de');

const FOOD = [
  {
    single: 'Kuchenstück',
    plural: 'Kuchenstücke',
    object: 'einen Kuchen, der',
    objectPlural: 'Kuchen',
    property: 'seinem Kuchen',
  },
  {
    single: 'Schokoladenstück',
    plural: 'Schokoladenstücke',
    object: 'eine Schokolade, die',
    objectPlural: 'Schokoladen',
    property: 'seiner Schokolade',
  },
  {
    single: 'Pizzastück',
    plural: 'Pizzastücken',
    object: 'eine Pizza, die',
    objectPlural: 'Pizzen',
    property: 'seiner Pizza',
  },
];

class VerbalTaskGenerator {
  static getExtensionTaskHelp(numerator, denumerator, factor) {
    const firstName = faker.name.firstName();
    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let food_name1 = numerator <= 1 ? food.single : food.plural;
    let food_name2 = denumerator <= 1 ? food.single : food.plural;
    const text = `Alle Kinder haben zusammen ${denumerator} ${food_name2}. ${firstName} hat ${numerator} ${food_name1}. Nun kaufen alle ${factor} mal so viele ${food_name2}. Wie viele hat ${firstName} (Zähler) und wie viele haben alle Kinder zusammen (Nenner)?`;
    return text;
  }

  static getShortenTaskHelp(numerator, denumerator, factor) {
    const firstName = faker.name.firstName();
    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let food_name1 = numerator <= 1 ? food.single : food.plural;
    let food_name2 = denumerator <= 1 ? food.single : food.plural;
    const text = `Alle Kinder haben zusammen ${denumerator} ${food_name2}. ${firstName} hat ${numerator} ${food_name1}. Wenn die Kinder ${factor} mal weniger ${food_name2} haben, wie viele hat ${firstName} (Zähler) und wie viele haben alle Kinder zusammen (Nenner)?`;
    return text;
  }

  static getSizeComparisonTaskHelp(
    numerator,
    denumerator,
    numerator2,
    denumerator2
  ) {
    const firstName = faker.name.firstName();
    const firstName2 = faker.name.firstName();

    if (firstName === firstName2) {
      return this.getSizeComparisonTaskHelp();
    }

    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let foodNameNum1 = numerator <= 1 ? food.single : food.plural;
    let foodNameDenum1 = denumerator <= 1 ? food.single : food.plural;
    let foodNameNum2 = numerator2 <= 1 ? food.single : food.plural;
    let foodNameDenum2 = denumerator2 <= 1 ? food.single : food.plural;
    let addTextNum = numerator === numerator2 ? ' auch ' : ' ';
    let addTextDenum = denumerator === denumerator2 ? ' auch ' : ' ';

    const text = `${firstName} (links) hat ${food.object} insgesamt ${denumerator} ${foodNameDenum1} hat. ${firstName2} (rechts) hat${addTextDenum} ${food.object} ${denumerator2} ${foodNameDenum2} hat. Die ${food.objectPlural} sind gleich groß. ${firstName} isst ${numerator} ${foodNameNum1} und ${firstName2} isst ${addTextNum}${numerator2} ${foodNameNum2}. Wer hat mehr von ${food.objectPlural} gegessen?`;
    return text;
  }

  static getOrderSizeTaskHelp(
    numerator,
    denumerator,
    numerator2,
    denumerator2,
    numerator3,
    denumerator3,
    numerator4,
    denumerator4
  ) {
    const firstName = faker.name.firstName();
    const firstName2 = faker.name.firstName();
    const firstName3 = faker.name.firstName();
    const firstName4 = faker.name.firstName();

    if (
      firstName === firstName2 ||
      firstName === firstName3 ||
      firstName === firstName4 ||
      firstName2 === firstName3 ||
      firstName2 === firstName4 ||
      firstName3 === firstName4
    ) {
      return this.getOrderSizeTaskHelp();
    }

    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let foodNameNum1 = numerator <= 1 ? food.single : food.plural;
    let foodNameDenum1 = denumerator <= 1 ? food.single : food.plural;
    let foodNameNum2 = numerator2 <= 1 ? food.single : food.plural;
    let foodNameDenum2 = denumerator2 <= 1 ? food.single : food.plural;
    let foodNameNum3 = numerator <= 1 ? food.single : food.plural;
    let FoodNameDenum3 = denumerator <= 1 ? food.single : food.plural;
    let FoodNameNum4 = numerator2 <= 1 ? food.single : food.plural;
    let FoodNameDenum4 = denumerator2 <= 1 ? food.single : food.plural;

    const text = `${firstName} (links) hat ${food.object} ${denumerator} ${foodNameDenum1} hat und isst ${numerator} ${foodNameNum1}. ${firstName2} (mitte links) hat ${food.object} ${denumerator2} ${foodNameDenum2} hat und isst ${numerator2} ${foodNameNum2}. ${firstName3} (mitte rechts) hat ${food.object} ${denumerator3} ${FoodNameDenum3} hat und isst ${numerator3} ${foodNameNum3}. ${firstName4} (rechts) hat ${food.object} ${denumerator4} ${FoodNameDenum4} hat und isst ${numerator4} ${FoodNameNum4}. Ordne danach wer am meisten von ${food.property} gegessen hat.`;
    return text;
  }

  static getAdditionTaskHelp(numerator, denumerator, numerator2, denumerator2) {
    const firstName = faker.name.firstName();
    const firstName2 = faker.name.firstName();

    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let foodNameNum1 = numerator <= 1 ? food.single : food.plural;
    let foodNameDenum1 = denumerator <= 1 ? food.single : food.plural;
    let foodNameNum2 = numerator2 <= 1 ? food.single : food.plural;
    let foodNameDenum2 = denumerator2 <= 1 ? food.single : food.plural;
    let addTextDenum = denumerator === denumerator2 ? ' auch ' : ' ';

    const text = `${firstName} (links) hat ${food.object} insgesamt ${denumerator} ${foodNameDenum1} hat. Davon nimmt sich ${firstName} ${numerator} ${foodNameNum1}. ${firstName2} (rechts) hat${addTextDenum} ${food.object} ${denumerator2} ${foodNameDenum2} hat. Die ${food.objectPlural} sind gleich groß. ${firstName2} gibt ${firstName} ${numerator2} ${foodNameNum2}. Wie viel gleich große ${food.plural} hat ${firstName} nun?`;
    return text;
  }

  static getSubtractionTaskHelp(
    numerator,
    denumerator,
    numerator2,
    denumerator2
  ) {
    const firstName = faker.name.firstName();
    const firstName2 = faker.name.firstName();

    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let foodNameNum1 = numerator <= 1 ? food.single : food.plural;
    let foodNameDenum1 = denumerator <= 1 ? food.single : food.plural;
    let foodNameDenum2 = denumerator2 <= 1 ? food.single : food.plural;
    let addTextDenum = denumerator === denumerator2 ? ' auch ' : ' ';

    const text = `${firstName} (links) hat ${food.object} insgesamt ${denumerator} ${foodNameDenum1} hat. Davon nimmt sich ${firstName} ${numerator} ${foodNameNum1}. ${firstName2} (rechts) hat${addTextDenum} ${food.object} ${denumerator2} ${foodNameDenum2} hat. Die ${food.objectPlural} sind gleich groß. ${firstName} gibt ${firstName2} ${numerator} ${foodNameNum1}. Wie viel gleich große ${food.plural} hat ${firstName} nun übrig?`;
    return text;
  }

  static getMultiplyTaskHelp(numerator, denumerator, numerator2, denumerator2) {
    const firstName = faker.name.firstName();

    const food = FOOD[parseInt(Math.random() * FOOD.length, 10)];
    let foodNameNum1 = numerator <= 1 ? food.single : food.plural;
    let foodNameNum2 = numerator2 <= 1 ? food.single : food.plural;
    let foodNameDenum1 = denumerator <= 1 ? food.single : food.plural;

    const text = `${firstName} hat ${food.object} insgesamt ${denumerator} ${foodNameDenum1} hat. Davon isst ${firstName} ${numerator} ${foodNameNum1}.  ${firstName} kauft nun ${denumerator2} mal so viele ${food.objectPlural} und isst ${numerator2} mal mehr ${foodNameNum2} als davor. Wie viel ${food.plural} hat ${firstName} (Zähler) gegessen und wie viele ${food.objectPlural} hat ${firstName} (Nenner) insgesamt kauft?`;
    return text;
  }
}

module.exports = VerbalTaskGenerator;
