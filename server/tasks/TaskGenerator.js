const VerbalTaskGenerator = require('./VerbalTaskGenerator');

class TaskGenerator {
  // SHORTEN TASKS
  static getSimpleShortenTask() {
    let shortenValue = parseInt(Math.random() * 3 + 1, 10) * 2;

    let numerator = 1; // Zähler

    const denumerator = parseInt(Math.random() * 6 + 1, 10) * shortenValue;
    numerator = shortenValue;

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getSimpleShortenTask();
    }

    const resultNumerator = numerator / shortenValue;
    const resultDenumerator = denumerator / shortenValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getShortenTaskHelp(
      numerator,
      denumerator,
      shortenValue
    );

    return {
      type: 'Simple shorten',
      numerator,
      denumerator,
      description: `Gekürzt mit ${shortenValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateShortenTask() {
    let shortenValue = parseInt(Math.random() * 4 + 3, 10);
    if (shortenValue % 2 === 0) {
      shortenValue += 1;
    }

    let numerator = 0; // Zähler
    let denumerator = 0; // Nenner

    // Get integer value between 2 and 10
    numerator = parseInt(Math.random() * 5 + 1, 10) * shortenValue;
    denumerator = parseInt(Math.random() * 10 + 1, 10) * shortenValue;

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getIntermediateShortenTask();
    }

    const resultNumerator = numerator / shortenValue;
    const resultDenumerator = denumerator / shortenValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getShortenTaskHelp(
      numerator,
      denumerator,
      shortenValue
    );

    return {
      type: 'Simple shorten',
      numerator,
      denumerator,
      description: `Gekürzt mit ${shortenValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardShortenTask() {
    let shortenValue = parseInt(Math.random() * 6 + 5, 10);
    if (shortenValue % 2 === 0) {
      shortenValue += 1;
    }

    let numerator = 0; // Zähler
    let denumerator = 0; // Nenner

    // Get integer value between 2 and 10
    numerator = parseInt(Math.random() * 10 + 1, 10) * shortenValue;
    denumerator = parseInt(Math.random() * 10 + 1, 10) * shortenValue;

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getHardShortenTask();
    }

    const resultNumerator = numerator / shortenValue;
    const resultDenumerator = denumerator / shortenValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getShortenTaskHelp(
      numerator,
      denumerator,
      shortenValue
    );

    return {
      type: 'Simple shorten',
      numerator,
      denumerator,
      description: `Gekürzt mit ${shortenValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  // EXTENSION TASKS
  static getSimpleExtensionTask() {
    const extensionValue = parseInt(Math.random() * 3 + 2, 10);

    const denumerator = parseInt(Math.random() * 5 + 1, 10) * 2;
    const numerator = 1;

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getSimpleExtensionTask();
    }

    const resultNumerator = numerator * extensionValue;
    const resultDenumerator = denumerator * extensionValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getExtensionTaskHelp(
      numerator,
      denumerator,
      extensionValue
    );
    return {
      type: 'Simple extension',
      numerator,
      denumerator,
      description: `Erweitert mit ${extensionValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateExtensionTask() {
    const extensionValue = parseInt(Math.random() * 6 + 5, 10);

    const numerator = parseInt(Math.random() * 5 + 1, 10);
    const denumerator = parseInt(Math.random() * 10 + 1, 10);

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getIntermediateExtensionTask();
    }
    const resultNumerator = numerator * extensionValue;
    const resultDenumerator = denumerator * extensionValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getExtensionTaskHelp(
      numerator,
      denumerator,
      extensionValue
    );

    return {
      type: 'Simple extension',
      numerator,
      denumerator,
      description: `Erweitert mit ${extensionValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardExtensionTask() {
    let extensionValue = parseInt(Math.random() * 11 + 5, 10);
    if (extensionValue % 2 === 0) {
      extensionValue += 1;
    }
    const numerator = parseInt(Math.random() * 8 + 3, 10);
    const denumerator = parseInt(Math.random() * 8 + 3, 10);

    if (numerator === denumerator || numerator / denumerator > 1) {
      return this.getHardExtensionTask();
    }

    const resultNumerator = numerator * extensionValue;
    const resultDenumerator = denumerator * extensionValue;

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getExtensionTaskHelp(
      numerator,
      denumerator,
      extensionValue
    );

    return {
      type: 'Simple extension',
      numerator,
      denumerator,
      description: `Erweitert mit ${extensionValue}`,
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  // SIZE COMPARISON TASKS
  static getSimpleSizeComparisonTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 3 + 1, 10);
    let numerator2 = parseInt(Math.random() * 3 + 1, 10);
    let denumerator = parseInt(Math.random() * 10 + 1, 10);
    let denumerator2 = parseInt(Math.random() * 10 + 1, 10);
    const random = Math.random();

    if (random > 0.5) {
      numerator = numerator2;
    } else {
      denumerator = denumerator2;
    }

    if (numerator === numerator2 && denumerator === denumerator2) {
      return this.getSimpleSizeComparisonTask();
    }

    // Fraction must be visualised in one pie os no fractions > 1
    if (numerator / denumerator > 1 || numerator2 / denumerator2 > 1) {
      return this.getSimpleSizeComparisonTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getSimpleSizeComparisonTask();
    }

    let result = 0;
    let diff = numerator / denumerator < numerator2 / denumerator2;
    if (diff) {
      result = 2;
    } else {
      result = 1;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSizeComparisonTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Size comparison task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: 'oder',
      verbalHelp,
      result,
    };
  }

  static getIntermediateSizeComparisonTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    let denumerator = 0;
    let denumerator2 = 0;
    const random = Math.random();
    const factor = parseInt(Math.random() * 3 + 1, 10);
    if (random >= 0.5) {
      denumerator = parseInt(Math.random() * 8 + 5, 10);
      denumerator2 = parseInt(factor * denumerator, 10);
    } else {
      denumerator2 = parseInt(Math.random() * 8 + 5, 10);
      denumerator = parseInt(factor * denumerator2, 10);
    }

    // Fraction must be visualised in one pie os no fractions > 1
    if (numerator / denumerator > 1 || numerator2 / denumerator2 > 1) {
      return this.getIntermediateSizeComparisonTask();
    }

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getIntermediateSizeComparisonTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getIntermediateSizeComparisonTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getIntermediateSizeComparisonTask();
    }

    let result = 0;
    let diff = numerator / denumerator < numerator2 / denumerator2;

    if (diff) {
      result = 2;
    } else {
      result = 1;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSizeComparisonTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );
    return {
      type: 'Size comparison task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: 'oder',
      verbalHelp,
      result,
    };
  }

  static getHardSizeComparisonTask() {
    // Calculate numerator and denumerator

    // Get factors for same div
    const factor1 = parseInt(Math.random() * 4 + 2, 10);
    const factor2 = parseInt(Math.random() * 4 + 2, 10);

    // Denumenator
    let denumeratorBase = parseInt(Math.random() * 6 + 5, 10);
    const denumerator = denumeratorBase * factor1;
    const denumerator2 = denumeratorBase * factor2;

    // Numenator
    const numerator = parseInt(Math.random() * 10 + 5, 10);
    const numerator2 = parseInt(Math.random() * 10 + 5, 10);

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getHardSizeComparisonTask();
    }

    // Fraction must be visualised in one pie os no fractions > 1
    if (numerator / denumerator > 1 || numerator2 / denumerator2 > 1) {
      return this.getHardSizeComparisonTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getHardSizeComparisonTask();
    }

    // We want both fractions to be extended or shorted in the task
    if (denumerator % denumerator2 === 0 || denumerator2 % denumerator === 0) {
      return this.getHardSizeComparisonTask();
    }

    // No single fractions bigger than one as it would be too easy
    if (
      (numerator / denumerator > 1 && numerator2 / denumerator2 < 1) ||
      (numerator / denumerator < 1 && numerator2 / denumerator2 > 1)
    ) {
      return this.getHardSizeComparisonTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getHardSizeComparisonTask();
    }

    let result = 0;
    let diff = numerator / denumerator < numerator2 / denumerator2;

    if (diff) {
      result = 2;
    } else {
      result = 1;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSizeComparisonTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Size comparison task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: 'oder',
      verbalHelp,
      result,
    };
  }

  // SIZE COMPARISON TASKS
  static getSimpleOrderSizeTask() {
    // Calculate numerator
    let numerator = parseInt(Math.random() * 10 + 1, 10);
    let numerator2 = parseInt(Math.random() * 10 + 1, 10);
    let numerator3 = parseInt(Math.random() * 10 + 1, 10);
    let numerator4 = parseInt(Math.random() * 10 + 1, 10);

    // Calculate denumerator
    let denumerator = parseInt(Math.random() * 9 + 2, 10);
    let denumerator2 = parseInt(Math.random() * 9 + 2, 10);
    let denumerator3 = denumerator;
    let denumerator4 = denumerator2;

    // Calculate results
    let result1 = numerator / denumerator;
    let result2 = numerator2 / denumerator2;
    let result3 = numerator3 / denumerator3;
    let result4 = numerator4 / denumerator4;

    // Check for duplicates
    const allResults = [result1, result2, result3, result4];
    if (this.hasDuplicates(allResults)) {
      return this.getSimpleOrderSizeTask();
    }

    // No fractions > 1
    if (result1 >= 1 || result2 >= 1 || result3 >= 1 || result4 >= 1) {
      return this.getSimpleOrderSizeTask();
    }

    // Build json list
    let list = [
      {
        id: 1,
        numerator,
        denumerator,
        result: result1,
      },
      {
        id: 2,
        numerator: numerator2,
        denumerator: denumerator2,
        result: result2,
      },
      {
        id: 3,
        numerator: numerator3,
        denumerator: denumerator3,
        result: result3,
      },
      {
        id: 4,
        numerator: numerator4,
        denumerator: denumerator4,
        result: result4,
      },
    ];

    // No duplicates are allowed
    list.sort((a, b) => a.result - b.result);

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getOrderSizeTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4
    );

    return {
      type: 'Order task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4,
      verbalHelp,
      result: list,
    };
  }

  static getIntermediateOrderSizeTask() {
    // Calculate numerator
    let numerator = parseInt(Math.random() * 9 + 1, 10);
    let numerator2 = parseInt(Math.random() * 9 + 1, 10);
    let numerator3 = parseInt(Math.random() * 9 + 1, 10);
    let numerator4 = parseInt(Math.random() * 9 + 1, 10);

    // Calculate denumerator
    let denumerator = parseInt(Math.random() * 7 + 4, 10);
    let denumerator2 = parseInt(Math.random() * 7 + 4, 10);
    let denumerator3 = parseInt(Math.random() * 7 + 4, 10);
    let denumerator4 = parseInt(Math.random() * 7 + 4, 10);

    // Skip if too much equal denumerators
    if (
      (denumerator === denumerator2 && denumerator2 === denumerator3) ||
      (denumerator2 === denumerator3 && denumerator2 === denumerator4) ||
      (denumerator3 === denumerator4 && denumerator3 === denumerator) ||
      (denumerator4 === denumerator && denumerator4 === denumerator2)
    ) {
      return this.getIntermediateOrderSizeTask();
    }

    // Calculate results
    let result1 = numerator / denumerator;
    let result2 = numerator2 / denumerator2;
    let result3 = numerator3 / denumerator3;
    let result4 = numerator4 / denumerator4;

    // Check for duplicates
    const allResults = [result1, result2, result3, result4];
    if (this.hasDuplicates(allResults)) {
      return this.getIntermediateOrderSizeTask();
    }

    // No fractions > 1
    if (result1 >= 1 || result2 >= 1 || result3 >= 1 || result4 >= 1) {
      return this.getIntermediateOrderSizeTask();
    }

    // Build json list
    let list = [
      {
        id: 1,
        numerator,
        denumerator,
        result: result1,
      },
      {
        id: 2,
        numerator: numerator2,
        denumerator: denumerator2,
        result: result2,
      },
      {
        id: 3,
        numerator: numerator3,
        denumerator: denumerator3,
        result: result3,
      },
      {
        id: 4,
        numerator: numerator4,
        denumerator: denumerator4,
        result: result4,
      },
    ];

    // No duplicates are allowed
    list.sort((a, b) => a.result - b.result);

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getOrderSizeTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4
    );

    return {
      type: 'Order task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4,
      verbalHelp,
      result: list,
    };
  }

  static getHardOrderSizeTask() {
    // Calculate numerator
    let numerator = parseInt(Math.random() * 6 + 5, 10);
    let numerator2 = parseInt(Math.random() * 6 + 5, 10);
    let numerator3 = parseInt(Math.random() * 6 + 5, 10);
    let numerator4 = parseInt(Math.random() * 6 + 5, 10);

    // Calculate denumerator
    let denumerator = parseInt(Math.random() * 15 + 10, 10);
    let denumerator2 = parseInt(Math.random() * 15 + 10, 10);
    let denumerator3 = parseInt(Math.random() * 15 + 10, 10);
    let denumerator4 = parseInt(Math.random() * 15 + 10, 10);

    // Skip if too much equal denumerators
    if (
      denumerator === denumerator2 ||
      denumerator === denumerator3 ||
      denumerator === denumerator4 ||
      denumerator2 === denumerator3 ||
      denumerator2 === denumerator4 ||
      denumerator3 === denumerator4
    ) {
      return this.getHardOrderSizeTask();
    }

    // Calculate results
    let result1 = numerator / denumerator;
    let result2 = numerator2 / denumerator2;
    let result3 = numerator3 / denumerator3;
    let result4 = numerator4 / denumerator4;

    // Check for duplicates
    const allResults = [result1, result2, result3, result4];
    if (this.hasDuplicates(allResults)) {
      return this.getHardOrderSizeTask();
    }

    // No fractions > 1
    if (result1 >= 1 || result2 >= 1 || result3 >= 1 || result4 >= 1) {
      return this.getHardOrderSizeTask();
    }

    // Build json list
    let list = [
      {
        id: 1,
        numerator,
        denumerator,
        result: result1,
      },
      {
        id: 2,
        numerator: numerator2,
        denumerator: denumerator2,
        result: result2,
      },
      {
        id: 3,
        numerator: numerator3,
        denumerator: denumerator3,
        result: result3,
      },
      {
        id: 4,
        numerator: numerator4,
        denumerator: denumerator4,
        result: result4,
      },
    ];

    // No duplicates are allowed
    list.sort((a, b) => a.result - b.result);

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getOrderSizeTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4
    );

    return {
      type: 'Order task',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      numerator3,
      denumerator3,
      numerator4,
      denumerator4,
      verbalHelp,
      result: list,
    };
  }

  // ADDITION TASKS
  static getSimpleAdditionTask() {
    const numerator = parseInt(Math.random() * 5 + 1, 10);
    const numerator2 = parseInt(Math.random() * 5 + 1, 10);

    if (numerator === numerator2) {
      return this.getSimpleAdditionTask();
    }

    // Simple tasks have the same denumenator
    const denumerator = parseInt(Math.random() * 9 + 2, 10);
    const denumerator2 = denumerator;

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getSimpleAdditionTask();
    }

    let resultNumerator = numerator + numerator2;
    let resultDenumerator = denumerator;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getAdditionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Addition task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '+',
      verbalHelp,
      factor1: 1,
      factor2: 1,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateAdditionTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    let denumerator = 0;
    let denumerator2 = 0;
    let resultNumerator = 0;
    let resultDenumerator = 0;

    const random = Math.random();
    const factor = parseInt(Math.random() * 3 + 1, 10);
    let factor1 = 1;
    let factor2 = 1;
    if (random >= 0.5) {
      denumerator = parseInt(Math.random() * 8 + 5, 10);
      denumerator2 = parseInt(factor * denumerator, 10);
      resultNumerator = numerator * factor + numerator2;
      resultDenumerator = denumerator2;
      factor1 = factor;
    } else {
      denumerator2 = parseInt(Math.random() * 8 + 5, 10);
      denumerator = parseInt(factor * denumerator2, 10);
      resultNumerator = numerator2 * factor + numerator;
      resultDenumerator = denumerator;
      factor2 = factor;
    }

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getIntermediateAdditionTask();
    }

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getIntermediateAdditionTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getIntermediateAdditionTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getIntermediateAdditionTask();
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getAdditionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Addition task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '+',
      verbalHelp,
      factor1,
      factor2,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardAdditionTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    // Get factors for same div
    const factor1 = parseInt(Math.random() * 4 + 2, 10);
    const factor2 = parseInt(Math.random() * 4 + 2, 10);

    // Denumenator
    let denumeratorBase = parseInt(Math.random() * 8 + 3, 10);
    const denumerator = denumeratorBase * factor1;
    const denumerator2 = denumeratorBase * factor2;

    // Results
    let resultNumerator = numerator * factor2 + numerator2 * factor1;
    let resultDenumerator = denumerator * factor2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getHardAdditionTask();
    }

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getHardAdditionTask();
    }

    // Too easy
    if (denumerator % denumerator2 === 0 || denumerator2 % denumerator === 0) {
      return this.getHardAdditionTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getHardAdditionTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getHardAdditionTask();
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getAdditionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Addition task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '+',
      verbalHelp,
      factor1: factor2,
      factor2: factor1,
      resultNumerator,
      resultDenumerator,
    };
  }

  // SUBTRACTION TASKS
  static getSimpleSubtractionTask() {
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    // Numenators may not be the same
    if (numerator === numerator2) {
      return this.getSimpleSubtractionTask();
    }

    // Switch numerator as the first must be bigger
    if (numerator2 > numerator) {
      const tmp = numerator;
      numerator = numerator2;
      numerator2 = tmp;
    }
    // Simple tasks have the same denumenator
    const denumerator = parseInt(Math.random() * 9 + 2, 10);
    const denumerator2 = denumerator;

    let resultNumerator = numerator - numerator2;
    let resultDenumerator = denumerator;

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getSimpleSubtractionTask();
    }

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSubtractionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Subtraction task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      factor1: 1,
      factor2: 1,
      description: '-',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateSubtractionTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 10 + 1, 10);
    let numerator2 = parseInt(Math.random() * 10 + 1, 10);

    let denumerator = 0;
    let denumerator2 = 0;
    let resultNumerator = 0;
    let resultDenumerator = 0;

    const random = Math.random();
    const factor = parseInt(Math.random() * 3 + 1, 10);
    let factor1 = 1;
    let factor2 = 1;
    if (random >= 0.5) {
      denumerator = parseInt(Math.random() * 6 + 5, 10);
      denumerator2 = parseInt(factor * denumerator, 10);
      resultNumerator = numerator * factor - numerator2;
      resultDenumerator = denumerator2;
      factor1 = factor;
    } else {
      denumerator2 = parseInt(Math.random() * 6 + 5, 10);
      denumerator = parseInt(factor * denumerator2, 10);
      resultNumerator = numerator2 * factor - numerator;
      resultDenumerator = denumerator;
      factor2 = factor;
    }

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getIntermediateSubtractionTask();
    }

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getIntermediateSubtractionTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getIntermediateSubtractionTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getIntermediateSubtractionTask();
    }

    if (numerator / denumerator < numerator2 / denumerator2) {
      return this.getIntermediateSubtractionTask();
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSubtractionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Subtraction task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      factor1,
      factor2,
      description: '-',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardSubtractionTask() {
    // Calculate numerator and denumerator
    let numerator = parseInt(Math.random() * 6 + 5, 10);
    let numerator2 = parseInt(Math.random() * 6 + 5, 10);

    // Get factors for same div
    const factor1 = parseInt(Math.random() * 4 + 2, 10);
    const factor2 = parseInt(Math.random() * 4 + 2, 10);

    // Denumenator
    let denumeratorBase = parseInt(Math.random() * 8 + 3, 10);
    const denumerator = denumeratorBase * factor1;
    const denumerator2 = denumeratorBase * factor2;

    // Results
    let resultNumerator = numerator * factor2 - numerator2 * factor1;
    let resultDenumerator = denumerator * factor2;

    // No fractions > 1
    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getHardSubtractionTask();
    }

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    if (numerator === numerator2 || denumerator === denumerator2) {
      return this.getHardSubtractionTask();
    }

    // Too easy
    if (denumerator % denumerator2 === 0 || denumerator2 % denumerator === 0) {
      return this.getHardSubtractionTask();
    }

    // Even decimal is too easy
    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getHardSubtractionTask();
    }

    // Same result is invalid
    if (numerator / denumerator === numerator2 / denumerator2) {
      return this.getHardSubtractionTask();
    }

    // First fraction must be bigger
    if (numerator / denumerator < numerator2 / denumerator2) {
      return this.getIntermediateSubtractionTask();
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getSubtractionTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Subtraction task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '-',
      verbalHelp,
      factor1,
      factor2,
      resultNumerator,
      resultDenumerator,
    };
  }

  // MULTIPLAY TASKS
  static getSimpleMultiplyTask() {
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 4 + 2, 10);
    let denumerator2 = parseInt(Math.random() * 4 + 2, 10);

    // Secure that you can shorten the fraction
    const random = Math.random();
    if (random > 0.5) {
      denumerator2 = numerator;
    } else {
      denumerator = numerator2;
    }

    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getSimpleMultiplyTask();
    }

    // Result
    let resultNumerator = numerator * numerator2;
    let resultDenumerator = denumerator * denumerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Multiply task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '*',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateMultiplyTask() {
    let numerator = parseInt(Math.random() * 8 + 3, 10);
    let numerator2 = parseInt(Math.random() * 8 + 3, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 8 + 3, 10);
    let denumerator2 = parseInt(Math.random() * 8 + 3, 10);

    // Only shorten one fraction
    if (numerator % denumerator2 === 0 && numerator2 % denumerator === 0) {
      return this.getIntermediateMultiplyTask();
    }

    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getIntermediateMultiplyTask();
    }

    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getIntermediateMultiplyTask();
    }

    // Result
    let resultNumerator = numerator * numerator2;
    let resultDenumerator = denumerator * denumerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Multiply task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '*',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardMultiplyTask() {
    let numerator = parseInt(Math.random() * 11 + 5, 10);
    let numerator2 = parseInt(Math.random() * 11 + 5, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 16 + 5, 10);
    let denumerator2 = parseInt(Math.random() * 16 + 5, 10);

    // The fraction should not be shorted
    if (
      numerator % denumerator2 === 0 ||
      numerator2 % denumerator === 0 ||
      numerator % denumerator === 0 ||
      numerator2 % denumerator2 === 0 ||
      denumerator % numerator === 0 ||
      denumerator2 % numerator2 === 0
    ) {
      return this.getHardMultiplyTask();
    }

    if (numerator === denumerator || numerator2 === denumerator2) {
      return this.getHardMultiplyTask();
    }

    if (numerator / denumerator >= 1 || numerator2 / denumerator2 >= 1) {
      return this.getHardMultiplyTask();
    }

    // Result
    let resultNumerator = numerator * numerator2;
    let resultDenumerator = denumerator * denumerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      numerator2,
      denumerator2
    );

    return {
      type: 'Multiply task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '*',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  // DIVISION TASKS
  static getSimpleDivisionTask() {
    let numerator = parseInt(Math.random() * 5 + 1, 10);
    let numerator2 = parseInt(Math.random() * 5 + 1, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 4 + 2, 10);
    let denumerator2 = parseInt(Math.random() * 4 + 2, 10);

    // Secure that you can shorten the fraction
    const random = Math.random();
    if (random > 0.5) {
      denumerator2 = numerator;
    } else {
      denumerator = numerator2;
    }

    if (numerator === denumerator || numerator2 <= denumerator2) {
      return this.getSimpleDivisionTask();
    }

    if (numerator / denumerator >= 1) {
      return this.getSimpleDivisionTask();
    }

    // Result
    let resultNumerator = numerator * denumerator2;
    let resultDenumerator = denumerator * numerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      denumerator2,
      numerator2
    );

    return {
      type: 'Division task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '/',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getIntermediateDivisionTask() {
    let numerator = parseInt(Math.random() * 8 + 3, 10);
    let numerator2 = parseInt(Math.random() * 8 + 3, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 8 + 3, 10);
    let denumerator2 = parseInt(Math.random() * 8 + 3, 10);

    // Only shorten one fraction
    if (
      (numerator % numerator2 === 0 && denumerator % denumerator2 === 0) ||
      (numerator2 % numerator === 0 && denumerator2 % denumerator === 0) ||
      (numerator % numerator2 === 0 && denumerator2 % denumerator === 0) ||
      (numerator2 % numerator === 0 && denumerator % denumerator2 === 0)
    ) {
      return this.getIntermediateDivisionTask();
    }

    if (numerator === denumerator || numerator2 <= denumerator2) {
      return this.getIntermediateDivisionTask();
    }

    if (numerator / denumerator >= 1) {
      return this.getIntermediateDivisionTask();
    }

    // Result
    let resultNumerator = numerator * numerator2;
    let resultDenumerator = denumerator * denumerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      denumerator2,
      numerator2
    );

    return {
      type: 'Division task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '/',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  static getHardDivisionTask() {
    let numerator = parseInt(Math.random() * 11 + 5, 10);
    let numerator2 = parseInt(Math.random() * 11 + 5, 10);

    // Simple tasks have the same denumenator
    let denumerator = parseInt(Math.random() * 16 + 5, 10);
    let denumerator2 = parseInt(Math.random() * 16 + 5, 10);

    // The fraction should not be shorted
    if (
      numerator % denumerator2 === 0 ||
      numerator2 % denumerator === 0 ||
      numerator % denumerator === 0 ||
      numerator2 % denumerator2 === 0 ||
      denumerator % numerator === 0 ||
      denumerator2 % numerator2 === 0 ||
      denumerator % numerator2 === 0 ||
      denumerator2 % numerator === 0 ||
      denumerator % denumerator2 === 0 ||
      denumerator2 % denumerator === 0 ||
      numerator % numerator2 === 0 ||
      numerator2 % numerator === 0
    ) {
      return this.getHardDivisionTask();
    }

    if (numerator / denumerator >= 1) {
      return this.getHardDivisionTask();
    }

    if (numerator === denumerator || numerator2 <= denumerator2) {
      return this.getHardDivisionTask();
    }

    // Result
    let resultNumerator = numerator * denumerator2;
    let resultDenumerator = denumerator * numerator2;

    // Shorten result
    const shortenFactor = this.shortenResult(
      resultNumerator,
      resultDenumerator
    );

    if (shortenFactor !== null) {
      resultNumerator /= shortenFactor;
      resultDenumerator /= shortenFactor;
    }

    // Get verbal help
    const verbalHelp = VerbalTaskGenerator.getMultiplyTaskHelp(
      numerator,
      denumerator,
      denumerator2,
      numerator2
    );

    return {
      type: 'Division task',
      hint: 'Brüche bitte vollständig Kürzen',
      numerator,
      denumerator,
      numerator2,
      denumerator2,
      description: '/',
      verbalHelp,
      resultNumerator,
      resultDenumerator,
    };
  }

  // HELPER

  static shortenResult(numenator, denumenator) {
    for (let i = numenator; i > 1; i -= 1) {
      if (denumenator % i === 0 && numenator % i === 0) {
        return i;
      }
    }
    return null;
  }

  static hasDuplicates(arr) {
    return new Set(arr).size !== arr.length;
  }
}

module.exports = TaskGenerator;
