// Imports
const mysql = require('mysql');

// Load config
const dbConfig = require('../config/database').db_config;

let clusterConfig = {
  removeNodeErrorCount: 1, // Remove the node immediately when connection fails.
  defaultSelector: 'ORDER',
};

let poolCluster;

let connections = [
  {
    name: 'user',
    config: dbConfig.user,
  },
];

/** Testing the given database connection
 *
 * @param {*} name Name of the connection
 */
function testConnection(name) {
  // if(process.env.NODE_ENV !== 'production') return;
  try {
    const pool = poolCluster.of(name);

    pool.getConnection((err, connection) => {
      if (err) {
        console.error(err);
      } else if (process.env.NODE_ENV !== 'test') {
        console.log(
          `Successfully connected to database as user '${connection.config.user}'`
        );
      }

      if (typeof connection !== 'undefined') connection.release();
    });
  } catch (err) {
    console.error(err);
  }
}

/**
 * Bundle method to start all sql connections
 */
const connect = () => {
  console.log('Connection to Database...');

  poolCluster = mysql.createPoolCluster(clusterConfig);

  poolCluster.on('remove', (nodeId) => {
    console.log(`REMOVED NODE : ${nodeId}`); // nodeId = SLAVE1
  });

  connections.forEach((con) => {
    // ADD CLUSTER
    poolCluster.add(con.name, con.config);

    // TEST CONNECTION
    testConnection(con.name);
  });
};

/** Helper function to start a sql connection, execute a given query and close the connection
 *
 * @param {*} query  SQL Query to be executed
 */
const executeQuery = (config, query, callback) => {
  if (!poolCluster) {
    console.log('No connection!');
    callback(null, null);
  }
  poolCluster.getConnection(config, (err, connection) => {
    if (err) {
      console.error(err.stack || err);
      console.error(connection);
    }
    connection.query(query, (err2, results) => {
      if (err2) {
        console.error(err2);
        console.error(query);
      }
      connection.release();
      callback(err2, results);
    });
  });
};

module.exports = { connect, executeQuery };
