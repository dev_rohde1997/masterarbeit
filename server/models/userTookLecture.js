// Import libs
const util = require('util');
const mysql = require('mysql');

// Load DB Connection Method
const db = require('../util/DBConnector');

const query = util.promisify(db.executeQuery);

/**
 * Model for lecture table
 *
 */
class UserTookLecture {
  constructor(object) {
    this.user_id = object.user_id;
    this.lecture_id = object.lecture_id;
    this.rating = object.rating;
    this.time = object.time;
  }

  /**
   * Get by id
   * @param {*} userLecture lecture object
   */
  static async getById(userId, lectureId) {
    const sql = mysql.format(
      'SELECT * from user_took_lecture WHERE user_id = ? AND lecture_id = ?;',
      [userId, lectureId]
    );
    return await query('user', sql);
  }

  /**
   * Add a new entry
   * @param {*} userLecture lecture object
   */
  static async add(userLecture) {
    const sql = mysql.format(
      'INSERT INTO user_took_lecture set ?;',
      userLecture
    );
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} lecture object
   * @param {*} name Name of the lecture
   */
  static async update(lecture, user_id, lecture_id) {
    const sql = mysql.format(
      'UPDATE user_took_lecture set ? WHERE user_id = ? AND lecture_id = ?;',
      [lecture, user_id, lecture_id]
    );
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} rating object
   * @param {*} user_id object
   * @param {*} lecture_id object
   */
  static async updateRating(rating, user_id, lecture_id) {
    const sql = mysql.format(
      'UPDATE user_took_lecture set rating = ? WHERE user_id = ? AND lecture_id = ?;',
      [rating, user_id, lecture_id]
    );
    return await query('user', sql);
  }
}

module.exports = UserTookLecture;
