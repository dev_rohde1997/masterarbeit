// Import libs
const util = require('util');
const mysql = require('mysql');

// Load DB Connection Method
const db = require('../util/DBConnector');

const query = util.promisify(db.executeQuery);

/**
 * Model for user table
 *
 */
class User {
  constructor(object) {
    this.name = object.name;
  }

  /**
   * Get one user by given ID
   * @async
   * @param {*} id Id of the user
   */
  static async getById(id) {
    const sql = mysql.format('SELECT * from user WHERE id = ?', id);
    return await query('user', sql);
  }

  /** user
   * Get one user by given ID
   * @async
   * @param {*} id Id of the user
   */
  static async getByName(id) {
    const sql = mysql.format('SELECT * from user WHERE name = ?', id);
    return await query('user', sql);
  }

  /**
   * Add a new entry
   * @param {*} user user object
   */
  static async add(user) {
    const sql = mysql.format('INSERT INTO user set ?;', user);
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} user user object
   * @param {*} name Name of the cash register
   */
  static async update(user, id) {
    const sql = mysql.format('UPDATE user set ? WHERE id =  ?;', [user, id]);
    return await query('user', sql);
  }
}

module.exports = User;
