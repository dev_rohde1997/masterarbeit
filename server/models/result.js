// Import libs
const util = require('util');
const mysql = require('mysql');

// Load DB Connection Method
const db = require('../util/DBConnector');

const query = util.promisify(db.executeQuery);

/**
 * Model for result table
 *
 */
class Result {
  constructor(object) {
    this.correct = object.correct;
    this.wrong = object.wrong;
    this.warning = object.warning;
    this.time = object.time;
    this.user_id = object.user_id;
    this.lecture_id = object.lecture_id;
  }

  /**
   * Get one result by given ID
   * @async
   * @param {*} id Id of the result
   */
  static async getById(id) {
    const sql = mysql.format(
      'SELECT r.id as resultId, r.correct, r.wrong, r.warning, r.user_id as userId, r.lecture_id as lectureId, r.time, l.name as lectureName from result r ' +
        'JOIN lecture l ON l.id = r.lecture_id ' +
        'WHERE r.id = ?',
      id
    );
    return await query('user', sql);
  }

  /**
   * Get one result by given user id
   * @async
   * @param {*} id Id of the user
   */
  static async getByUserId(userId) {
    const sql = mysql.format(
      'SELECT * from result r ' +
        'JOIN lecture l ON l.id = r.lecture_id ' +
        'WHERE r.user_id = ? ' +
        'ORDER BY r.id DESC',
      userId
    );
    return await query('user', sql);
  }

  /**
   * Add a new entry
   * @param {*} result result object
   */
  static async add(result) {
    const sql = mysql.format('INSERT INTO result set ?;', result);
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} result result object
   * @param {*} name Name of the cash register
   */
  static async update(result, id) {
    const sql = mysql.format('UPDATE result set ? WHERE id =  ?;', [
      result,
      id,
    ]);
    return await query('user', sql);
  }
}

module.exports = Result;
