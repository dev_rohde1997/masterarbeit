// Import libs
const util = require('util');
const mysql = require('mysql');

// Load DB Connection Method
const db = require('../util/DBConnector');

const query = util.promisify(db.executeQuery);

/**
 * Model for learning_style table
 *
 */
class LearningStyle {
  constructor(object) {
    this.user_id = object.user_id;
    this.sensing = object.sensing;
    this.intuitive = object.intuitive;
    this.visual = object.visual;
    this.verbal = object.verbal;
    this.active = object.active;
    this.reflective = object.reflective;
    this.sequential = object.sequential;
    this.global = object.global;
  }

  /**
   * Get one learning_style by given ID
   * @async
   * @param {*} id Id of the user
   */
  static async getById(id) {
    const sql = mysql.format(
      'SELECT * from learning_style WHERE user_id = ?',
      id
    );
    return await query('user', sql);
  }

  /**
   * Add a new entry
   * @param {*} learning_style learning_style object
   */
  static async add(learning_style) {
    const sql = mysql.format(
      'INSERT INTO learning_style set ?;',
      learning_style
    );
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} learning_style learning_style object
   * @param {*} name Name of the cash register
   */
  static async update(learning_style, id) {
    const sql = mysql.format(
      'UPDATE learning_style set ? WHERE user_id =  ?;',
      [learning_style, id]
    );
    return await query('user', sql);
  }
}

module.exports = LearningStyle;
