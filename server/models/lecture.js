// Import libs
const util = require('util');
const mysql = require('mysql');

// Load DB Connection Method
const db = require('../util/DBConnector');

const query = util.promisify(db.executeQuery);

/**
 * Model for lecture table
 *
 */
class Lecture {
  constructor(object) {
    this.name = object.name;
  }

  /**
   * Get one lecture by given ID
   * @async
   * @param {*} id Id of the lecture
   */
  static async getAllByUserId(id) {
    const sql = mysql.format(
      'SELECT u.name as userName, u.id as userId, l.name as lectureName, l.id as lectureId, rating from lecture l ' +
        'JOIN user_took_lecture ul ON ul.lecture_id = l.id ' +
        'JOIN user u ON u.id = ul.user_id ' +
        'WHERE u.id = ?',
      id
    );
    return await query('user', sql);
  }

  /**
   * Get one lecture by given ID
   * @async
   * @param {*} id Id of the lecture
   */
  static async getById(id) {
    const sql = mysql.format('SELECT * from lecture WHERE id = ?', id);
    return await query('user', sql);
  }

  /**
   * Get one lecture by given ID
   * @async
   * @param {*} id Id of the lecture
   */
  static async getAll() {
    const sql = mysql.format('SELECT * from lecture');
    return await query('user', sql);
  }

  /**
   * Add a new entry
   * @param {*} lecture lecture object
   */
  static async add(lecture) {
    const sql = mysql.format('INSERT INTO lecture set ?;', lecture);
    return await query('user', sql);
  }

  /**
   * Update an entry
   * @param {*} lecture object
   * @param {*} name Name of the lecture
   */
  static async update(lecture, id) {
    const sql = mysql.format('UPDATE lecture set ? WHERE id = ?;', [
      lecture,
      id,
    ]);
    return await query('user', sql);
  }
}

module.exports = Lecture;
