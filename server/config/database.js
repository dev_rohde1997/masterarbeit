let db_config = {
  user: {
    host: process.env.DATABASE_NAME,
    port: '3306',
    user: 'user',
    password: 'user',
    database: 'database',
  },
};

module.exports = { db_config };
