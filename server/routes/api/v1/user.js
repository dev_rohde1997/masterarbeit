const express = require('express');

const router = express.Router();
const UserController = require('../../../controller/UserController');

router.get('/:id', UserController.get);
router.post('/add', UserController.add);
router.post('/login', UserController.login);

module.exports = router;
