const express = require('express');

const router = express.Router();
const ResultController = require('../../../controller/ResultController');

router.get('/:id', ResultController.get);
router.post('/:id/add', ResultController.add);

module.exports = router;
