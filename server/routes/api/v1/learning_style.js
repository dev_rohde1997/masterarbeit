const express = require('express');

const router = express.Router();
const learningStyleController = require('../../../controller/LearningStyleController');

router.get('/:id', learningStyleController.get);
router.post('/add', learningStyleController.add);

module.exports = router;
