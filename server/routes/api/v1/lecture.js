const express = require('express');

const router = express.Router();
const LectureController = require('../../../controller/LectureController');

router.get('/:lectureId/users/:userId', LectureController.getTask);

router.get('/overview/:userId', LectureController.getAll);

module.exports = router;
