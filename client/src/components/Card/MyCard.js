import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import { ButtonBase, CardActionArea, CardHeader } from "@mui/material";
import Button from "@mui/material/Button";
import { Link as RouterLink } from 'react-router-dom'

export default function MyCard(props) {
  return (
    <Card raised={true}>
      <CardActionArea component={RouterLink} to={props.path}>
      <CardContent>
        <Typography variant="h5">{props.title}</Typography>
        <CardMedia
          sx={{mt: 1}}
          component="img"
          height={props.height}
          image={props.image}
          alt="image"
        />
      </CardContent>
      </CardActionArea>
    </Card>
  );
}
