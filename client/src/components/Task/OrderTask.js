import { Typography, Stack, Grid, Divider, Button } from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useEffect, useState } from "react";
import DragFraction from "./DragFraction";
import Fraction from "./Fraction";

import HoverFraction from "./HoverFraction";
import ResultFraction from "./ResultFraction";

const DropArea = (props) => {

  function drop(ev) {
    ev.preventDefault();

    const mNumerator = ev.dataTransfer.getData("numenator");
    const mDenumenator = ev.dataTransfer.getData("denumenator");

    // Check values
    props.evaluateResult(props.id, mNumerator, mDenumenator);
  }

  function allowDrop(ev) {
    ev.preventDefault();
  }


  return (
    <Grid xs={2} item >

      <div onDrop={drop} onDragOver={allowDrop} style={{ backgroundColor: props.backgroundColor, border: "dashed black 2px", borderRadius: '5px' }}>
        <Stack
          padding={1}
          spacing={2}
          divider={<Divider variant="middle" textAlign="center" sx={{ background: "black" }} />}
        >
          <Typography align="center">{props.answer.numerator}</Typography>
          <Typography align="center">{props.answer.denumerator}</Typography>

        </Stack>
      </div>
    </Grid>
  )
}

const BiggerSign = () => {
  return (
    <Grid item xs={1}>
      <Typography variant="h5">{"<"}</Typography>
    </Grid>
  )
}

const Space = () => <Grid xs={1} item ></Grid>

export default function OrderTask(props) {

  return (
    <div>
      <Grid
        spacing={4}
        alignItems="center"
        justifyContent="center"
        container>
        <Grid
          xs={12}
          item
        >
          <Typography align="center" variant="subtitle1">Sortiere die Brüche aufsteigend</Typography>
        </Grid>
        <Grid xs={2} item >
          <DragFraction mNumerator={props.mNumerator} mDenumerator={props.mDenumerator} />
        </Grid>
        <Space />
        <Grid xs={2} item >
          <DragFraction mNumerator={props.mNumerator2} mDenumerator={props.mDenumerator2} />
        </Grid>
        <Space />
        <Grid xs={2} item >
          <DragFraction mNumerator={props.mNumerator3} mDenumerator={props.mDenumerator3} />
        </Grid>
        <Space />
        <Grid xs={2} item >
          <DragFraction mNumerator={props.mNumerator4} mDenumerator={props.mDenumerator4} />
        </Grid>
        <DropArea id={1} evaluateResult={props.evaluateResult} backgroundColor={props.backgroundColor1} answer={props.answer1}  />
        <BiggerSign />
        <DropArea id={2} evaluateResult={props.evaluateResult} backgroundColor={props.backgroundColor2} answer={props.answer2}/>
        <BiggerSign />
        <DropArea id={3} evaluateResult={props.evaluateResult} backgroundColor={props.backgroundColor3} answer={props.answer3}/>
        <BiggerSign />
        <DropArea id={4} evaluateResult={props.evaluateResult} backgroundColor={props.backgroundColor4} answer={props.answer4}/>
      </Grid>
    </div>
  );
}
