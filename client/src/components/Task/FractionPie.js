import { Typography, Stack, Grid, Divider } from "@mui/material";
import { blue, green, grey, indigo, red } from "@mui/material/colors";
import React, { useEffect, useState } from "react";
import { PieChart, Pie, Sector, Cell, Legend, ResponsiveContainer } from 'recharts';

export default function FractionPie(props) {

  const [numerator, setNumerator] = useState();
  const [denumerator, setdenumerator] = useState();
  const [data, setData] = useState([]);
  const [fillColor, setFillColor] = useState([]);
  const [size, setSize] = useState(props.size);

  useEffect(() => {
    setNumerator(props.numerator);
    setdenumerator(props.denumerator);
  })

  useEffect(() => {
    let tmpData = [];
    let tmpColors = [];
    for (let i = 0; i < denumerator; i += 1) {
      tmpData.push({
        name: i,
        value: 100,
      });
      if (i < numerator) {
        tmpColors.push(blue[700]);
      } else {
        tmpColors.push('white')
      }
    }
    setFillColor(tmpColors);
    setData(tmpData);
  }, [numerator, denumerator])

  return (

    <ResponsiveContainer height={size*3}>
      <PieChart>
        <Pie
          data={data}
          outerRadius={size}
          startAngle={90}
          endAngle={-270}
          dataKey="value"
          animationDuration={0}
          stroke={"black"}
          strokeWidth={1}
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={fillColor[index]} />
          ))}

        </Pie>
      </PieChart >
    </ResponsiveContainer >
  );
}