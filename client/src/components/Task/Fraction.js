import { Typography, Stack, Grid, Divider } from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useEffect, useState } from "react";


export default function Fraction(props) {
  return (
    <Stack
      spacing={2}
      divider={<Divider variant="middle" textAlign="center" sx={{ background: "black" }} />}
    >
      <Typography align="center">{props.mNumerator}</Typography>
      <Typography align="center">{props.mDenumerator}</Typography>

    </Stack>
  );
}