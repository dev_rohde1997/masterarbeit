import { Typography, Stack, Grid, Divider, FormControl, TextField } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";


export default function ResultFraction(props) {

  return (
    <FormControl>

      <Stack
        spacing={2}
        divider={<Divider variant="middle" textAlign="center" sx={{ background: "black" }} />}
      >
        <TextField
          required
          autoFocus
          disabled={props.disabled}
          autoComplete="off"
          inputRef={props.refNumenator}
          label="Zähler"
          variant="outlined"
        />
        <TextField 
          required
          autoComplete="off"
          disabled={props.disabled}
          label="Nenner"
          inputRef={props.refDenumenator}
          variant="outlined"
        />

      </Stack>
    </FormControl>
  );
}