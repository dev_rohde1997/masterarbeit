import { Typography, Stack, Grid, Divider } from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useEffect, useState } from "react";


export default function HoverFraction(props) {
  return (
    <Stack
      spacing={2}
      paddingLeft={3}
      paddingRight={3}
      sx={[
        {
          '&:hover': {
            backgroundColor: grey[200],
          },
        },
      ]}
      id={props.id}
      onClick={props.fractionClick}
      divider={<Divider id={props.id} variant="middle" textAlign="center" sx={{ background: "black"}}/>}
    >
      <Typography id={props.id} align="center">{props.mNumerator}</Typography>
      <Typography id={props.id} align="center">{props.mDenumerator}</Typography>

    </Stack>
  );
}