import { Typography, Stack, Grid, Divider, Button } from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useEffect, useState } from "react";

import HoverFraction from "./HoverFraction";
import ResultFraction from "./ResultFraction";

export default function SizeComparisonTask(props) {

  return (
    <div>
      <Grid
        spacing={2}
        alignItems="center"
        justifyContent="center"
        container>
        <Grid
          xs={12}
          item
        >
          <Typography align="center" variant="subtitle1">Klicke auf den größeren Bruch</Typography>
        </Grid>
        <Grid xs={2} item ></Grid>
        <Grid xs={2} item >
          <HoverFraction mNumerator={props.mNumerator} mDenumerator={props.mDenumerator} fractionClick={props.fractionClick} id={1}/>
        </Grid>
        <Grid item xs={4}>
          <Typography align="center" variant="h6">{props.description}</Typography>
        </Grid>
        <Grid xs={2} item>
          <HoverFraction mNumerator={props.mNumerator2} mDenumerator={props.mDenumerator2} fractionClick={props.fractionClick} id={2}/>
        </Grid>
        <Grid xs={2} item ></Grid>
        

      </Grid>
    </div>
  );
}
