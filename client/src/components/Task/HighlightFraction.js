import { Typography, Stack, Grid, Divider } from "@mui/material";
import { blueGrey, grey, lightGreen, red } from "@mui/material/colors";
import React, { useEffect, useState } from "react";


export default function HighlightFraction(props) {
  return (
    <div style={{ border: "solid 2px", borderRadius: '5px' }}>
      <Stack
        backgroundColor={red[200]}
        padding={0.8}
        spacing={2}
        divider={<Divider variant="middle" textAlign="center" sx={{ background: "black" }} />}
      >
        <Typography align="center">{props.mNumerator}</Typography>
        <Typography align="center">{props.mDenumerator}</Typography>

      </Stack>
    </div>
  );
}