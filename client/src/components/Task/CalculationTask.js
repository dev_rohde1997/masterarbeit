import { Typography, Stack, Grid, Box, Button, Icon, Container, Divider } from "@mui/material";
import React, { useEffect, useState } from "react";

import Fraction from "./Fraction";
import ResultFraction from "./ResultFraction";
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import LensIcon from '@mui/icons-material/Lens';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import { red } from "@mui/material/colors";

const ArithmeticOperator = (props) => {
  switch (props.operator) {
    case "-":
      return (
        <Grid
          spacing={2}
          alignItems="center"
          justifyContent="center"
          container>
          <Grid item xs={0}>
            <RemoveIcon/>
          </Grid>
        </Grid>
      );
    case "+":
      return (
        <Grid
          alignItems="center"
          justifyContent="center"
          container>
          <Grid item xs={0}>
            <AddIcon/>
          </Grid>
        </Grid>
      );
    default:
    case "*":
      return (
        <Grid
          alignItems="center"
          justifyContent="center"
          container>
          <Grid item xs={0}>
            <CloseIcon/>
          </Grid>
        </Grid>
      );
    case "/":
      return (
        <Grid
          spacing={2}
          alignItems="center"
          justifyContent="center"
          container>
          <Grid item xs={0}>
            <Typography align="center" variant="h4">÷</Typography>
          </Grid>
        </Grid>
      );
  }
  return (
    <Box />
  );
}

export default function CalculationTask(props) {



  return (
    <div>
      <Grid
        spacing={2}
        alignItems="center"
        justifyContent="center"
        container>
        <Grid item xs={1}>
          <Fraction mNumerator={props.mNumerator} mDenumerator={props.mDenumerator} />
        </Grid>
        <Grid item xs={3} style={{ textAlign: "center" }}>
          <ArithmeticOperator operator={props.description} />
        </Grid>
        <Grid item xs={1}>
          <Fraction mNumerator={props.mNumerator2} mDenumerator={props.mDenumerator2} />
        </Grid>
        <Grid item xs={2} style={{ textAlign: "center" }}>
          <Icon>
            <DragHandleIcon />
          </Icon>
        </Grid>
        <Grid item xs={3}>
          <ResultFraction
            disabled={props.disabled}
            refNumenator={props.refNumenator}
            refDenumenator={props.refDenumenator}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography fontWeight="fontWeightLight" align="center">{props.hint}</Typography>
        </Grid>
      </Grid>
    </div>
  );
}
