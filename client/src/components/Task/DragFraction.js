import { Typography, Stack, Grid, Divider } from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useEffect, useState } from "react";
import { useDrag } from 'react-dnd'

export default function DragFraction(props) {
 
  function drag(ev) {
    ev.dataTransfer.setData("numenator", props.mNumerator);
    ev.dataTransfer.setData("denumenator", props.mDenumerator);
  }

  return (
     <div onDragStart={drag} draggable={true} style={{  border: "solid black 2px", borderRadius: '5px' }}>
      <Stack
      padding={1}
      sx={{ '&:hover': { cursor: 'grab', background: 'lightgrey' } }}
        spacing={2}
        divider={<Divider variant="middle" textAlign="center" sx={{ background: "black" }} />}
      >
        <Typography align="center">{props.mNumerator}</Typography>
        <Typography align="center">{props.mDenumerator}</Typography>

      </Stack>
    </div>
  )
}