import { Typography, Stack, Grid, Divider, Icon } from "@mui/material";
import React, { useEffect, useState } from "react";

import Fraction from "./Fraction";
import ResultFraction from "./ResultFraction";
import DragHandleIcon from '@mui/icons-material/DragHandle';

export default function ExtensionShortenTask(props) {

  return (
    <div>
      <Grid
        spacing={2}
        alignItems="center"
        justifyContent="center"
        container>
        <Grid item xs={1}>
          <Fraction mNumerator={props.mNumerator} mDenumerator={props.mDenumerator} />
        </Grid>
        <Grid item xs={5}>
          <Typography variant="h6" align="center">{props.description}</Typography>
        </Grid>
        <Grid item xs={2} style={{ textAlign: "center" }}>
          <Icon>
            <DragHandleIcon />
          </Icon>
        </Grid>
        <Grid item xs={3}>
          <ResultFraction
            disabled={props.disabled}
            refNumenator={props.refNumenator}
            refDenumenator={props.refDenumenator}
          />
        </Grid>
      </Grid>
    </div>
  );
}
