import * as React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { CssBaseline } from "@mui/material";
import Header from "./components/header/Header";
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";

// Load components
import Home from "./pages/Home/Home";
import Training from "./pages/Training/Training";
import Profile from "./pages/Profile/Profile";
import Results from "./pages/Results/Results";
import ILS from "./pages/LearningStyleTest/ILS";
import Course from "./pages/Training/Course";
import Login from "./pages/Login/Login";

const mdTheme = createTheme();

export default function App() {
  return (
    <ThemeProvider theme={mdTheme}>
      <CssBaseline>
        <Router>
          <Header></Header>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/home" element={<Home />} />
            <Route path="/lectures" element={<Training />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/results" element={<Results />} />
            <Route path="/learning-style" element={<ILS />} />
            <Route path="/lectures/:id" element={<Course />} />
          </Routes>
        </Router>
      </CssBaseline>
    </ThemeProvider>
  );
}
