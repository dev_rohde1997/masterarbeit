import React, { useEffect, useState, PureComponent } from "react";
import { Typography, Paper, Stack, Box, Card, CardHeader, CardContent, Grid, Container, Button } from "@mui/material";
import { PieChart, Pie, Sector, Cell, Legend } from 'recharts';
import { height, padding, width } from "@mui/system";
import { indigo, green } from "@mui/material/colors";

// const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const COLORS = [indigo[500], green[500]];


/*
https://nanopdf.com/download/felder-amp-solomon-8-l-university-of-bradford_pdf
*/
const MyPieCard = (props) => {
  return (
    <Paper elevation={3}>
      <Typography variant="h5" align="center" sx={{ marginTop: 2 }}>
        {props.headline}
      </Typography>
      <Grid container spacing={4}>
        <Grid item xs={0}>
          <PieChart width={250} height={300}>
            <Pie
              data={props.data}
              innerRadius={60}
              outerRadius={80}
              paddingAngle={5}
              dataKey="value"
              label
            >
              {props.data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index]} />
              ))}

            </Pie>
            <Legend verticalAlign="bottom" height={36} />
          </PieChart>
        </Grid>
        <Grid item xs={7}>
          <Typography sx={{ marginTop: 5 }}>
            {props.text}
          </Typography>
        </Grid>
      </Grid>
    </Paper>


  );
}

// PREFERNECE
const LOW_PREFERNECE_TEXT = (style) => `Du hast eine geringe Vorliebe für ${style}. `;
const MIDDLE_PREFERENCE_TEXT = (style) => `Du  hast eine gewisse Vorliebe für ${style}. In der richtigen Umgebung wird dir das Lernen leichter fallen. `;
const STRONG_PREFERENCE_TEXT = (style) => `Du hast eine starke Vorliebe für ${style}. Damit könntest du in einem unpassenden Umfeld echte Schwierigkeiten bekommen. `;

// STYLE TEXT
const SENSING_TEXT = "Faktisch veranlagte Lernende neigen dazu, gerne Fakten zu lernen. Deine Probleme löst du gerne mit bewährten Methoden, wobei du keine Komplikationen und Überraschungen magst. Wahrscheinlich neigst du dazu, geduldig mit Details umzugehen und hast eine Stärke praktische Arbeit zu leisten.";
const INTUITIVE_TEXT = "Intuitiv veranlagte Lernende bevorzugen oft das Entdecken von Möglichkeiten und Beziehungen. Deine Probleme löst du gerne durch Innovationen und magst Wiederholungen nicht besonders gerne. Wahrscheinlich kannst neue Konzepte besser erfassen als andere und fühlst dich mit Abstraktionen und mathematischen Formulierungen vertraut. Deine Arbeitsweise zeichnet sich durch Schnelligkeit und Innovation aus.";

const ACTIVE_TEXT = `Aktiv veranlagte Lernende neigen dazu, Informationen am besten zu behalten und zu verstehen, wenn sie aktiv etwas damit tun z.B. durch Diskussion, Anwendung oder Erklärung gegenüber anderen. Dein Motto könnte sein: "Probieren wir es aus und sehen wir, wie es funktioniert". Wahrscheinlich arbeitest du lieber in einer Gruppe als alleine. `;
const REFLECTIVE_TEXT = `Reflektierend veranlagte Lernende ziehen es vor zuerst in Ruhe über Dinge nachdenken. Dein Motto könnte sein: "Erstmal lieber darüber nachdenken". Wahrscheinlich lernst du lieber alleine als in einer Gruppe.`;

const VERBAL_TEXT = "Verbal veranlagte Lernende profitieren mehr von Worten, z. B. von schriftlichen und mündlichen Erklärungen. Grundsätzlich lernst mehr, wenn Informationen sowohl visuell als auch verbal präsentiert werden.";
const VISUAL_TEXT = "Visuell veranlagte Lernende merken sich am besten, was sie sehen, z. B. Bilder, Diagramme, Flussdiagramme, Zeitleisten, Filme, Demonstrationen usw. Grundsätzlich lernst mehr, wenn Informationen sowohl visuell als auch verbal präsentiert werden.";

const GLOBAL_TEXT = 'Global veranlagte Lernende neigen dazu, in großen Sprüngen zu lernen, indem sie den Stoff fast wahllos aufnehmen, ohne Zusammenhänge zu erkennen, und es dann plötzlich "kapieren". Du kannst komplexe Probleme schnell lösen und Dinge auf neuartige Weise zusammensetzen, sobald du das große Ganze erfasst hast. Allerdings kann es vorkommen, dass du Schwierigkeiten hast zu erklären, wie du das gemacht hast.';
const SEQUENTIAL_TEXT = "Sequentiell veranlagte Lernende neigen dazu, sich das Wissen in linearen Schritten anzueignen, wobei jeder Schritt logisch auf den vorhergehenden folgt. Du neigst dazu, bei der Suche nach Lösungen logischen Schritten zu folgen. ";


export default class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data1: [],
      data2: [],
      data3: [],
      data4: [],
      text1: "",
      text2: "",
      text3: "",
      text4: "",
      userId: null,
    };
  }

  componentDidMount() {

    this.loadData();
  }

  componentDidUpdate() {

    // Sensing vs. Intuitive
    const text1 = this.generateText(this.state.data1);
    this.setState({ text1 })

    const text2 = this.generateText(this.state.data2);
    this.setState({ text2 })

    const text3 = this.generateText(this.state.data3);
    this.setState({ text3 })

    const text4 = this.generateText(this.state.data4);
    this.setState({ text4 })

  }

  generateText(data) {
    let text = "";
    if (data !== null && data.length === 2) {
      const styleOne = data[0];
      const styleTwo = data[1];
      const diff = styleOne.value - styleTwo.value;
      const dominantStyle = diff > 0 ? styleOne : styleTwo;


      switch (dominantStyle.name) {
        case "Faktisch":
          text += this.getFirstText(diff, "faktisches Lernen");
          text += SENSING_TEXT;
          break;
        case "Intuitiv":
          text += this.getFirstText(diff, "intuitives Lernen");
          text += INTUITIVE_TEXT;
          break;
        case "Visuell":
          text += this.getFirstText(diff, "visuelles Lernen");
          text += VISUAL_TEXT;
          break;
        case "Verbal":
          text += this.getFirstText(diff, "verbales Lernen");
          text += VERBAL_TEXT
          break;
        case "Aktiv":
          text += this.getFirstText(diff, "aktives Lernen");
          text += ACTIVE_TEXT;
          break;
        case "Reflektierend":
          text += this.getFirstText(diff, "reflektierendes Lernen");
          text += REFLECTIVE_TEXT;
          break;
        case "Sequentiell":
          text += this.getFirstText(diff, "sequentielles Lernen");
          text += SEQUENTIAL_TEXT;
          break;
        case "Global":
          text += this.getFirstText(diff, "globales Lernen");
          text += GLOBAL_TEXT;
          break;
      }
    }
    return text;
  }


  getFirstText(diff, name) {
    let text = "";
    if (Math.abs(diff) < 4) {
      text += LOW_PREFERNECE_TEXT(name);
    } else if (Math.abs(diff) < 8) {
      text += MIDDLE_PREFERENCE_TEXT(name);
    } else {
      text += STRONG_PREFERENCE_TEXT(name);
    }

    return text;
  }

  loadData() {
    const storageUserId = sessionStorage.getItem('userId');
    this.setState({ userId: storageUserId })
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/learning-style/${storageUserId}`,
      { crossDomain: true })
      .then(response => response.json())
      .then(response => {
        const newData = response.data;
        let dataList1 = [];
        let dataList2 = [];
        let dataList3 = [];
        let dataList4 = [];
        for (const index in newData) {
          const name = Object.keys(newData[index])[0];
          const value = newData[index][name];
          switch (parseInt(index, 10)) {
            case 0:
            case 1: dataList1.push({
              name,
              value
            });
              break;
            case 2:
            case 3: dataList2.push({
              name,
              value
            });
              break;
            case 4:
            case 5: dataList3.push({
              name,
              value
            });
              break;
            case 6:
            case 7: dataList4.push({
              name,
              value
            });
              break;

          }
        }
        this.setState({ data1: dataList1 })
        this.setState({ data2: dataList2 })
        this.setState({ data3: dataList3 })
        this.setState({ data4: dataList4 })
      })
      .catch(error => console.error("Error:", error));
  }


  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: "5em",
        }}
      >
        <Paper elevation={0} sx={{ width: 700 }}>
          <Typography variant="h2" align="center">Dein Profil</Typography>
          <Typography variant="h6" align="center">Auswertung - Index der Lernstile style</Typography>

          {
            this.state.data1.length === 0  ?
              <Paper elevation={3} sx={{ marginTop: 3, paddingBottom: 4 }}>
                <Grid
                  spacing={2}
                  alignItems="center"
                  justifyContent="center"
                  container
                >
                  <Grid item xs={12}>
                    <Typography mt={3} align="center" variant="h6">Keine Daten vorhanden</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Button variant="contained" href="/learning-style" fullWidth>Zum Lernstiltest</Button>
                  </Grid>
                </Grid>
              </Paper>
              :
              <Stack sx={{ marginTop: 5 }} spacing={4}>

                <MyPieCard data={this.state.data1} text={this.state.text1} headline="Faktisch vs. Intuition"></MyPieCard>
                <MyPieCard data={this.state.data2} text={this.state.text2} headline="Visuell vs. Verbal"></MyPieCard>
                <MyPieCard data={this.state.data3} text={this.state.text3} headline="Aktiv vs. Reflektierend"></MyPieCard>
                <MyPieCard data={this.state.data4} text={this.state.text4} headline="Sequentiell vs. Global"></MyPieCard>

              </Stack>
          }

        </Paper>
      </div >
    );
  }
}