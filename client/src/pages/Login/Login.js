import React, { useEffect, useRef, useState } from "react";
import { Typography, Paper, Button, Input, InputLabel, FormHelperText, TextField } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Box } from "@mui/system";
export default function Login() {

  const [userId, setUserId] = useState(null);
  const [open, setOpen] = useState(false);
  const [newUser, setNewUser] = useState(false);
  const [hint, setHint] = useState('Bitte gib deinen Nutzernamen ein.')
  
  // Ref
  const userInput = useRef();

  useEffect(() => {
    const userId = sessionStorage.key("userId");
    if (userId !== null) {
      document.location.href = "/home";
    } else {
      setUserId(-1);
    }
  })

  function doLogin() {
    const userName = userInput.current.value;
    const body = JSON.stringify({ name: userName });
    // Get usernmae
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/users/login`, {
      crossDomain: true,
      method: "POST",
      body,
      headers: {
        "Content-Type": "application/json"
      },
    })
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            sessionStorage.setItem("userId", json.userId);
            sessionStorage.setItem("userName", json.name);
            document.location.href = '/home';
          });
        } else {
          setHint('Nutzer nicht gefunden');
          setOpen(true);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        setHint('Nutzer nicht gefunden');
      })
  }

  useEffect(newUser => {
    setNewUser(false);
    if (newUser) {
      handleCloseYesOption();
    }
  })

  function handleSubmit(event) {
    event.preventDefault();
    doLogin();
  }

  function handleCloseNoOption() {
    setOpen(false);
  };

  function handleClose(option) {
    if (option.target.id === 'dialog-btn-yes') {
      handleCloseYesOption();
    } else {
      handleCloseNoOption();
    }
  };

  function handleCloseYesOption() {
    const userName = userInput.current.value;
    const body = JSON.stringify({ name: userName });

    // Escape empty username
    if (userName === "") return;

    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/users/add`, {
      method: "POST",
      body,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            //console.log(json);
            sessionStorage.setItem("userId", json.userId);
            sessionStorage.setItem("userName", json.name);
            document.location.href = '/home';
          });
        } else {
          setHint('Nutzer konnte nicht erstellt werden');
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        setHint('Nutzer konnte nicht erstellt werden');
      })
    setOpen(false);

  };

  return (

    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      {
        userId === null ?
          <Box />
          :
          <Paper elevation={0} sx={{ width: 700 }}>
            <Typography variant="h4" align="center">Login</Typography>
            <Card sx={{ mt: 2 }}>
              <CardContent>
                <form onSubmit={handleSubmit}>
                  <FormControl fullWidth={true} required={true} focused={true}>
                    <TextField
                      required
                      autoFocus
                      autoComplete="off"
                      inputRef={userInput}
                      variant="standard"
                      label="Nutzername"
                    />
                    <Button
                      type="submit"
                      variant="contained"
                      sx={{ mt: 1.5, width: '100%' }}>
                      Login
                    </Button>
                  </FormControl>
                </form>
              </CardContent>
            </Card>
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                {"Nutzername leider nicht gefunden"}
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Neuen Nutzer erstellen?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button id="dialog-btn-no" onClick={handleClose}>Nein</Button>
                <Button id="dialog-btn-yes" onClick={handleClose} autoFocus>
                  Ja, Nutzer erstellen
                </Button>
              </DialogActions>
            </Dialog>
          </Paper >
      }
    </div >
  );
}