import React, { useEffect, useState } from "react";
import {
  Box,
  Container,
  CssBaseline,
  Typography,
  Grid,
  Paper,
  Stack,
  Button,
  Alert,
} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

// Import questions
import json from "../../config/ILS.json";

import { styled } from "@mui/material/styles";

export default function ILS() {
  const [questions, setQuestions] = React.useState(json.ILS);
  const [data, setData] = React.useState([]);
  const [userId, setUserId] = useState(0);

  useEffect(() => {
    // Get user
    setUserId(sessionStorage.getItem("userId"));
  });

  const submitForm = (event) => {
    event.preventDefault();

    const payload = { userId, data };

    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/learning-style/add`, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => setQuestions([]))
      .catch(error => console.error("Error:", error));
  };

  const handleQuestionChange = (event) => {
    const value = event.target.value;
    const questionNr = value.split('.')[0];
    const answerNr = value.split('.')[1];
    const tmpData = data;
    tmpData[questionNr] = parseInt(answerNr, 10);
    setData(tmpData);
  }

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      <CssBaseline />
      <Container maxWidth="md">
        <Paper elevation={3} sx={{ padding: "2em" }}>
          <Typography variant="h3" gutterBottom="true" align="center">
            Index der Lernstile
          </Typography>
          <React.Fragment>
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              <Box sx={{ flex: "1 1 auto" }} />
            </Box>
            <Stack spacing={2}>
              <FormControl component="fieldset">

                {questions.map((item) => (
                  <Paper
                    elevation={3}
                    sx={{ padding: "1em", margin: "0.5em" }}
                  >
                    <Typography variant="h6">
                      {item.id}. {item.question}
                    </Typography>
                    <RadioGroup
                      aria-label={item.question}
                      name="radio-buttons-group"
                      onChange={handleQuestionChange}
                    >
                      <FormControlLabel
                        value={item.id + ".1"}
                        control={<Radio />}
                        label={item.answer1}
                      />
                      <FormControlLabel
                        value={item.id + ".2"}
                        control={<Radio />}
                        label={item.answer2}
                      />
                    </RadioGroup>
                  </Paper>
                ))}
                {questions.length === 0 ?
                  <Container>
                    <Alert severity="success" sx={{ mr: 1, ml: 1, mt: 1.5 }}>Absenden erfolgreich!</Alert>
                    <Grid container spacing={4} sx={{ pr: 1 }}>
                      <Grid item xs={6}>
                        <Button variant="contained" href="/" sx={{ ml: 1, mt: 1.5, width: '100%' }}>
                          Zum Dashboard
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button variant="contained" href="/profile" sx={{ mt: 1.5, width: '100%' }}>
                          Zum Profil
                        </Button>
                      </Grid>
                    </Grid>


                  </Container> :
                  <Button variant="contained" onClick={submitForm} sx={{ mr: 1, ml: 1, mt: 1.5 }}>
                    Absenden
                  </Button>
                }
              </FormControl>
            </Stack>
          </React.Fragment>
        </Paper>
      </Container>
    </div>
  );
}
