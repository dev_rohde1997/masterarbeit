import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";

import MyCard from "../../components/Card/MyCard";

// Import logos
import trainingLogo from "../../img/learning-preview.jpg";
import profileLogo from "../../img/profile-picture.jpg";
import testLogo from "../../img/test-preview.jpg";
import resultLogo from "../../img/result-preview.jpg";

export default function Home() {

  const [userName, setUserName] = useState('User');

  useEffect(() => {
    const storageUserName = sessionStorage.getItem('userName');
    if (storageUserName !== null) {
      setUserName(storageUserName);
    }
  })
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      <Grid
        spacing={2}
        width="60%"
        container>
        <Grid item xs={12}>
          <Typography variant="h2" align="center">Willkommen, {userName}</Typography>
        </Grid>

        <Grid item xs={12}>
          <MyCard
            title="Training starten"
            image={trainingLogo}
            path="/lectures"
            height="130"
          />
        </Grid>
        <Grid item xs={4}>
          <MyCard
            title="Lernstil ermitteln"
            image={testLogo}
            path="/learning-style"
            height="130"
          />
        </Grid>
        <Grid item xs={4}>
          <MyCard
            title="Deine Ergebnisse"
            image={resultLogo}
            path="/results"
            height="130"
          />
        </Grid>
        <Grid item xs={4}>
          <MyCard
            title="Dein Profil"
            image={profileLogo}
            path="/profile"
            height="130"
          />
        </Grid>
      </Grid>
    </div>
  );
}
