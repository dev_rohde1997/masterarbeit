import React, { useEffect, useState, useRef } from "react";
import {
  Paper,
  Stack,
  Typography,
  Card,
  Container,
  CardHeader,
  CardContent,
  Divider,
  IconButton,
  Grid,
  Icon,
  Button
} from "@mui/material";
import { useParams } from "react-router-dom";
import { Box, display, fontWeight } from "@mui/system";
import ExtensionShortenTask from "../../components/Task/ExtensionShortenTask";
import CalculationTask from "../../components/Task/CalculationTask";
import EastRounded from '@mui/icons-material/EastRounded';
import DoneOutlined from '@mui/icons-material/DoneOutlined';
import WarningIcon from '@mui/icons-material/Warning';
import Error from '@mui/icons-material/Error';
import Fraction from "../../components/Task/Fraction";
import SizeComparisonTask from "../../components/Task/SizeComparisonTask";
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import { red } from "@mui/material/colors";
import OrderTask from "../../components/Task/OrderTask";
import HighlightFraction from "../../components/Task/HighlightFraction";
import FractionPie from "../../components/Task/FractionPie";
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import CloseIcon from '@mui/icons-material/Close';


export default function Course() {
  // Loaded values
  const { id } = useParams();
  const [title, setTitle] = useState();
  const [userId, setUserId] = useState();
  const [taskCounter, setTaskCounter] = useState(0);
  const [maxTasks, setMaxTasks] = useState(10);
  const [tasks, setTasks] = useState([]);
  const [learningStyle, setLearningStyle] = useState(null);

  // Result counter
  const [correctValueCount, setCorrectValueCount] = useState(0);
  const [wrongValueCount, setWrongValueCount] = useState(0);
  const [warningValueCount, setWarningValueCount] = useState(0);

  // States
  const [showResult, setShowResult] = useState(false);
  const [lectureComplete, setLectureComplete] = useState(false);
  const [waiting, setWaiting] = useState(false);
  const [showNextButton, setShowNextButton] = useState(true);
  const [answerNumenator, setAnswerNumanator] = useState(0);
  const [answerDenumenator, setAnswerDenumanator] = useState(0);
  const [halfCorrect, setHalfCorrect] = useState(false);
  const [showHelp, setShowHelp] = useState(true);

  // Order tasks
  const [orderTaskIds, setOrderTaskIds] = useState([]);
  const [backgroundColor1, setBackgroundColor1] = useState('#e5e8e8');
  const [backgroundColor2, setBackgroundColor2] = useState('#e5e8e8');
  const [backgroundColor3, setBackgroundColor3] = useState('#e5e8e8');
  const [backgroundColor4, setBackgroundColor4] = useState('#e5e8e8');
  const [answer1, setAnswer1] = useState({ numerator: '?', denumerator: '?' });
  const [answer2, setAnswer2] = useState({ numerator: '?', denumerator: '?' });
  const [answer3, setAnswer3] = useState({ numerator: '?', denumerator: '?' });
  const [answer4, setAnswer4] = useState({ numerator: '?', denumerator: '?' });

  // Result refs
  const resultRefNumenator = useRef();
  const resultRefDenumenator = useRef();

  useEffect(() => {
    // Get user
    setUserId(sessionStorage.getItem("userId"));
  });

  useEffect(() => {
    if (typeof userId === "undefined") return;
    // Load data
    loadData();
  }, [userId]);

  useEffect(() => {
    if (id === 2) {
      setShowNextButton(false);
    }
  }, [id])

  const loadData = () => {
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/lectures/${id}/users/${userId}`,
      { crossDomain: true })
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            console.log(json);
            setTitle(json.name);
            setTaskCounter(0);
            setTasks(json.tasks);
            setLearningStyle(json.learningStyle)
            setMaxTasks(json.tasks.length)
          })
        }
      })
      .catch(err => {
        console.log(err);
      })
  }

  const TaskLayout = (props) => {
    if (tasks.length === 0) return <Box />
    switch (parseInt(props.id)) {
      case 1:
        return (
          <ExtensionShortenTask
            mNumerator={tasks[taskCounter].numerator}
            mDenumerator={tasks[taskCounter].denumerator}
            description={tasks[taskCounter].description}
            refNumenator={resultRefNumenator}
            refDenumenator={resultRefDenumenator}
            disabled={showResult}
          />
        );
      case 2:
        switch (tasks[taskCounter].type) {
          case 'Order task':
            return (
              <OrderTask
                mNumerator={tasks[taskCounter].numerator}
                mDenumerator={tasks[taskCounter].denumerator}
                mNumerator2={tasks[taskCounter].numerator2}
                mDenumerator2={tasks[taskCounter].denumerator2}
                mNumerator3={tasks[taskCounter].numerator3}
                mDenumerator3={tasks[taskCounter].denumerator3}
                mNumerator4={tasks[taskCounter].numerator4}
                mDenumerator4={tasks[taskCounter].denumerator4}
                evaluateResult={evaluateOrderTask}
                backgroundColor1={backgroundColor1}
                backgroundColor2={backgroundColor2}
                backgroundColor3={backgroundColor3}
                backgroundColor4={backgroundColor4}
                answer1={answer1}
                answer2={answer2}
                answer3={answer3}
                answer4={answer4}
              />
            );
          case 'Size comparison task':
            return (
              <SizeComparisonTask
                mNumerator={tasks[taskCounter].numerator}
                mDenumerator={tasks[taskCounter].denumerator}
                mNumerator2={tasks[taskCounter].numerator2}
                mDenumerator2={tasks[taskCounter].denumerator2}
                description={tasks[taskCounter].description}
                fractionClick={fractionClick}
              />
            );
        }
      case 3:
        return (
          <CalculationTask
            mNumerator={tasks[taskCounter].numerator}
            mDenumerator={tasks[taskCounter].denumerator}
            mNumerator2={tasks[taskCounter].numerator2}
            mDenumerator2={tasks[taskCounter].denumerator2}
            description={tasks[taskCounter].description}
            refNumenator={resultRefNumenator}
            refDenumenator={resultRefDenumenator}
            hint={tasks[taskCounter].hint}
            disabled={showResult}
          />
        );
      case 4:
        return (
          <CalculationTask
            mNumerator={tasks[taskCounter].numerator}
            mDenumerator={tasks[taskCounter].denumerator}
            mNumerator2={tasks[taskCounter].numerator2}
            mDenumerator2={tasks[taskCounter].denumerator2}
            description={tasks[taskCounter].description}
            refNumenator={resultRefNumenator}
            refDenumenator={resultRefDenumenator}
            hint={tasks[taskCounter].hint}
            disabled={showResult}
          />
        );
      default:
        return (
          <Box />
        )

    }
  }

  const ResultLayout = (props) => {
    if (tasks.length <= 0 || !showResult) return <Box />
    switch (parseInt(props.id)) {
      case 1:
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <Typography variant="h6">Deine Antwort:</Typography>
            </Grid>
            <Grid item xs={0.7} color={red[500]}>
              <Fraction
                mNumerator={answerNumenator}
                mDenumerator={answerDenumenator}
              />
            </Grid>
            <Grid item xs={3}>
              <Typography variant="h6">Lösung:</Typography>
            </Grid>
            <Grid item xs={0.7}>
              <Fraction
                mNumerator={tasks[taskCounter].resultNumerator}
                mDenumerator={tasks[taskCounter].resultDenumerator}
              />
            </Grid>
            <Grid item xs={1}></Grid>
          </Grid>
        );
      case 2:
        switch (tasks[taskCounter].type) {
          case 'Order task':
            return (
              <Grid
                container
                spacing={2}
                mt={2}
                align="center"
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={12}>
                  <Typography variant="h6">Lösung:</Typography>
                </Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={1} >
                  <Fraction mNumerator={tasks[taskCounter].result[0].numerator} mDenumerator={tasks[taskCounter].result[0].denumerator} />
                </Grid>
                <Grid item xs={1}>
                  <Typography variant="h5">{"<"}</Typography>
                </Grid>
                <Grid item xs={1} >
                  <Fraction mNumerator={tasks[taskCounter].result[1].numerator} mDenumerator={tasks[taskCounter].result[1].denumerator} />
                </Grid>
                <Grid item xs={1}>
                  <Typography variant="h5">{"<"}</Typography>
                </Grid>
                <Grid item xs={1} >
                  <Fraction mNumerator={tasks[taskCounter].result[2].numerator} mDenumerator={tasks[taskCounter].result[2].denumerator} />
                </Grid>
                <Grid item xs={1}>
                  <Typography variant="h5">{"<"}</Typography>
                </Grid>
                <Grid item xs={1} >
                  <Fraction mNumerator={tasks[taskCounter].result[3].numerator} mDenumerator={tasks[taskCounter].result[3].denumerator} />
                </Grid>
                <Grid item xs={1}></Grid>

              </Grid>
            );
          case 'Size comparison task':
            return (
              <Grid
                container
                spacing={2}
                mt={2}
                align="center"
                alignItems="center"
                justifyContent="center"
              >
                <Grid item xs={12}>
                  <Fraction />
                  <Typography variant="h6">Leider Falsch!</Typography>
                </Grid>
                {
                  tasks[taskCounter].numerator === 1 ?
                    <Grid
                      container
                      spacing={2}
                      mt={2}
                      align="center"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item xs={1} >
                        <HighlightFraction mNumerator={tasks[taskCounter].numerator} mDenumerator={tasks[taskCounter].denumerator} />
                      </Grid>
                      <Grid item xs={2} >
                        <Typography variant="h5">{"<"}</Typography>
                      </Grid>
                      <Grid item xs={0.7} >
                        <Fraction mNumerator={tasks[taskCounter].numerator2} mDenumerator={tasks[taskCounter].denumerator2} />
                      </Grid>
                    </Grid>
                    :
                    <Grid
                      container
                      spacing={2}
                      mt={2}
                      align="center"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item xs={0.7} >
                        <Fraction mNumerator={tasks[taskCounter].numerator} mDenumerator={tasks[taskCounter].denumerator} />
                      </Grid>
                      <Grid item xs={2} >
                        <Typography variant="h5">{">"}</Typography>
                      </Grid>
                      <Grid item xs={1} >
                        <HighlightFraction mNumerator={tasks[taskCounter].numerator2} mDenumerator={tasks[taskCounter].denumerator2} />
                      </Grid>
                    </Grid>
                }

              </Grid>
            );
        }
      case 3:
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <Typography variant="h6">Deine Antwort:</Typography>
            </Grid>
            <Grid item xs={0.7} color={red[500]}>
              <Fraction
                mNumerator={answerNumenator}
                mDenumerator={answerDenumenator}
              />
            </Grid>
            <Grid item xs={3}>
              <Typography variant="h6">Lösung:</Typography>
            </Grid>
            <Grid item xs={0.7}>
              <Fraction
                mNumerator={tasks[taskCounter].resultNumerator}
                mDenumerator={tasks[taskCounter].resultDenumerator}
              />
            </Grid>
            <Grid item xs={1}></Grid>
          </Grid>
        );
      case 4:
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <Typography variant="h6">Deine Antwort:</Typography>
            </Grid>
            <Grid item xs={0.7} color={red[500]}>
              <Fraction
                mNumerator={answerNumenator}
                mDenumerator={answerDenumenator}
              />
            </Grid>
            <Grid item xs={3}>
              <Typography variant="h6">Lösung:</Typography>
            </Grid>
            <Grid item xs={0.7}>
              <Fraction
                mNumerator={tasks[taskCounter].resultNumerator}
                mDenumerator={tasks[taskCounter].resultDenumerator}
              />
            </Grid>
            <Grid item xs={1}></Grid>
          </Grid>
        );
      default:
        return (
          <Box />
        )
    }
  }

  const HelperLayout = () => {
    console.log(learningStyle);
    if (learningStyle === 'verbal') {
      return (
        <VerbalHelperLayout
          text={tasks[taskCounter].verbalHelp}
          type={tasks[taskCounter].type}
        />
      )
    } else if (learningStyle === 'visual') {
      return (
        <VisualHelperLayout
          numerator={tasks[taskCounter].numerator}
          denumerator={tasks[taskCounter].denumerator}
          description={tasks[taskCounter].description}
          numerator2={tasks[taskCounter].numerator2}
          denumerator2={tasks[taskCounter].denumerator2}
          numerator3={tasks[taskCounter].numerator3}
          denumerator3={tasks[taskCounter].denumerator3}
          numerator4={tasks[taskCounter].numerator4}
          denumerator4={tasks[taskCounter].denumerator4}
          factor1={tasks[taskCounter].factor1}
          factor2={tasks[taskCounter].factor2}
          type={tasks[taskCounter].type}
        />
      )
    } else {
      return <Box />
    }
  }

  const VerbalHelperLayout = (props) => {
    return (
      <Grid
        container
        spacing={2}
        mt={1}
        alignItems="center"
        justifyContent="center"
        align="center"
      >
        <Grid item xs={12}>
          <Typography variant="body1" align="center">{props.text}</Typography>
        </Grid>
      </Grid>
    );
  }

  const VisualHelperLayout = (props) => {
    switch (props.type) {
      case 'Simple extension':
        const factorE = props.description.substring(props.description.length - 2, props.description.length)
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <FractionPie numerator={props.numerator} denumerator={props.denumerator} size={50} />
            </Grid>
            <Grid item xs={7}>
              <Typography>Wir brauchen nun {factorE} mal so viele blaue und weiße viele Flächen. Wie viele sind blau (Zähler) und wie viele Flächen gibt es insgesamt (Nenner)?</Typography>
            </Grid>
          </Grid>
        );
      case 'Simple shorten':
        const factorS = props.description.substring(props.description.length - 2, props.description.length);
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <FractionPie numerator={props.numerator} denumerator={props.denumerator} size={50} />
            </Grid>
            <Grid item xs={1}>
            </Grid>
            <Grid item xs={8}>
              <Typography>Wir brauchen nun {factorS} mal weniger blaue und weiße viele Flächen. Wie viele sind blau (Zähler) und wie viele Flächen gibt es insgesamt (Nenner)?</Typography>
            </Grid>
          </Grid>
        );
      case 'Size comparison task':
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={6}>
              <FractionPie numerator={props.numerator} denumerator={props.denumerator} size={60} />
            </Grid>
            <Grid item xs={6}>
              <FractionPie numerator={props.numerator2} denumerator={props.denumerator2} size={60} />
            </Grid>
          </Grid>
        );
      case 'Order task':
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3}>
              <FractionPie
                numerator={props.numerator}
                denumerator={props.denumerator}
                size={40} />
            </Grid>
            <Grid item xs={3}>
              <FractionPie
                numerator={props.numerator2}
                denumerator={props.denumerator2}
                size={40} />
            </Grid>
            <Grid item xs={3}>
              <FractionPie
                numerator={props.numerator3}
                denumerator={props.denumerator3}
                size={40} />
            </Grid>
            <Grid item xs={3}>
              <FractionPie
                numerator={props.numerator4}
                denumerator={props.denumerator4}
                size={40} />
            </Grid>
            <Grid xs={1.5} item >
              <Fraction mNumerator={props.numerator} mDenumerator={props.denumerator} />
            </Grid>
            <Grid xs={1.5} item ></Grid>
            <Grid xs={1.5} item >
              <Fraction mNumerator={props.numerator2} mDenumerator={props.denumerator2} />
            </Grid>
            <Grid xs={1.5} item ></Grid>
            <Grid xs={1.5} item >
              <Fraction mNumerator={props.numerator3} mDenumerator={props.denumerator3} />
            </Grid>
            <Grid xs={1.5} item ></Grid>
            <Grid xs={1.5} item >
              <Fraction mNumerator={props.numerator4} mDenumerator={props.denumerator4} />
            </Grid>
          </Grid>
        );
      case 'Addition task':
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={5}>
              <FractionPie
                numerator={props.numerator * props.factor1}
                denumerator={props.denumerator * props.factor1}
                size={80} />
            </Grid>
            <Grid item xs={2}>
              <Icon>
                <AddIcon />
              </Icon>
            </Grid>
            <Grid item xs={5}>
              <FractionPie
                numerator={props.numerator2 * props.factor2}
                denumerator={props.denumerator2 * props.factor2}
                size={80} />
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Zähler: Addiere die blauen Flächen miteinander
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Nenner: Die Gesamtzahl der Flächen
              </Typography>
            </Grid>
          </Grid>

        );
      case 'Subtraction task':
        return (
          <Grid
            container
            spacing={2}
            align="center"
            mt={1}
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={4}>
              <FractionPie
                numerator={props.numerator * props.factor1}
                denumerator={props.denumerator * props.factor1}
                size={80} />
            </Grid>
            <Grid item xs={4}>
              <Icon>
                <RemoveIcon />
              </Icon>
            </Grid>
            <Grid item xs={4}>
              <FractionPie
                numerator={props.numerator2 * props.factor2}
                denumerator={props.denumerator2 * props.factor2}
                size={80} />
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Zähler: Ziehe von den linken blauen Flächen die Anzahl der rechten ab
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Nenner: Die Gesamtzahl der Flächen
              </Typography>
            </Grid>
          </Grid>
        );
      case 'Multiply task':
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={4}>
              <FractionPie
                numerator={props.numerator}
                denumerator={props.denumerator}
                size={70} />
            </Grid>
            <Grid item xs={4}>
              <Icon>
                <CloseIcon />
              </Icon>
            </Grid>
            <Grid item xs={4}>
              <FractionPie
                numerator={props.numerator2}
                denumerator={props.denumerator2}
                size={70} />
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Zähler: Multipliziere die blauen Flächen miteinander
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Nenner: Multipliziere die Gesamtzahl der Flächen
              </Typography>
            </Grid>
          </Grid>
        );
      case 'Division task':
        return (
          <Grid
            container
            spacing={2}
            mt={1}
            align="center"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={4}>
              <FractionPie
                numerator={props.numerator}
                denumerator={props.denumerator}
                size={70} />
            </Grid>
            <Grid item xs={4}>
              <Icon>
                <CloseIcon />
              </Icon>
            </Grid>
            <Grid item xs={4}>
              <FractionPie
                numerator={props.denumerator2}
                denumerator={props.numerator2}
                size={70} />
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Zähler: Multipliziere die blauen Flächen miteinander
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography align="center" variant="h6">
                Nenner: Multipliziere die Gesamtzahl der Flächen
              </Typography>
            </Grid>
          </Grid>
        );
      default:
        return (
          <Typography>{props.type}</Typography>
        );
    }

  }

  useEffect(() => {
    if (typeof userId === "undefined") return;
    if (lectureComplete) {
      setLectureComplete(false);
      return;
    }

    if (taskCounter < tasks.length - 1) {
      setTaskCounter(taskCounter + 1);
      setWaiting(false);
    } else {
      setWaiting(true);
      setTimeout(() => {
        setLectureComplete(true);
        setWaiting(false);
      }, 2000);

      sendResults();
    }
  }, [wrongValueCount, correctValueCount, warningValueCount])

  const sendResults = () => {
    const body = {
      lectureId: id,
      correctCount: correctValueCount,
      wrongCount: wrongValueCount,
      warningCount: warningValueCount,
    }
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/results/${userId}/add`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            // console.log(json);
          });
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      })
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!waiting) {
      checkResult();
    }
  }

  const evaluateOrderTask = (id, numeratorAnswer, denumenatorAnswer) => {
    const updatedList = orderTaskIds;
    updatedList.push(id);
    setOrderTaskIds(updatedList);


    const json = { numerator: numeratorAnswer, denumerator: denumenatorAnswer };

    switch (id) {
      case 1: setBackgroundColor1('white');
        setAnswer1(json);
        break;
      case 2: setBackgroundColor2('white');
        setAnswer2(json);
        break;
      case 3: setBackgroundColor3('white');
        setAnswer3(json);
        break;
      case 4: setBackgroundColor4('white');
        setAnswer4(json);
        break;
    }
  }

  const evaluateOrderTaskResult = () => {
    // Check if all fields are filled
    let validationOK = true;
    for (let i = 1; i < 5; i++) {
      let found = false;
      for (const id of orderTaskIds) {
        if (parseInt(id, 10) === i) {
          found = true;
        }
      }

      if (!found) {
        validationOK = false;
        // Mark field as required
        switch (i) {
          case 1: setBackgroundColor1('red');
            break;
          case 2: setBackgroundColor2('red');
            break;
          case 3: setBackgroundColor3('red');
            break;
          case 4: setBackgroundColor4('red');
            break;
        }
      }
    }

    // Cancel check if not all fields are filled
    if (!validationOK) {
      return;
    }

    // Check result
    const result = tasks[taskCounter].result;

    let allCorrect = true;
    if (parseInt(answer1.numerator) !== parseInt(result[0].numerator) || parseInt(answer1.denumerator) !== parseInt(result[0].denumerator)) {
      setBackgroundColor1('red');
      allCorrect = false;
    }

    if (parseInt(answer2.numerator) !== parseInt(result[1].numerator) || parseInt(answer2.denumerator) !== parseInt(result[1].denumerator)) {
      setBackgroundColor2('red');
      allCorrect = false;
    }

    if (parseInt(answer3.numerator) !== parseInt(result[2].numerator) || parseInt(answer3.denumerator) !== parseInt(result[2].denumerator)) {
      setBackgroundColor3('red');
      allCorrect = false;
    }

    if (parseInt(answer4.numerator) !== parseInt(result[3].numerator) || parseInt(answer4.denumerator) !== parseInt(result[3].denumerator)) {
      setBackgroundColor4('red');
      allCorrect = false;
    }

    if (allCorrect) {
      setBackgroundColor1('green');
      setBackgroundColor2('green');
      setBackgroundColor3('green');
      setBackgroundColor4('green');
      setTimeout(() => {
        resetOrderSizeElements();
        setCorrectValueCount(correctValueCount + 1);
      }, 1000);

    } else {
      setShowResult(true);
    }
  }

  const checkResult = () => {


    const taskType = tasks[taskCounter].type;

    if (taskType === 'Order task') {
      evaluateOrderTaskResult();
      return;
    }

    if (typeof resultRefNumenator.current === "undefined" ||
      typeof resultRefDenumenator.current === "undefined") {
        return;
      }

      const numerator = resultRefNumenator.current.value;
    const denumerator = resultRefDenumenator.current.value;

    setAnswerNumanator(numerator);
    setAnswerDenumanator(denumerator);

    const denumenatorInt = parseInt(denumerator, 10);
    const numenatorInt = parseInt(numerator, 10);
    const denumenatorResInt = parseInt(tasks[taskCounter].resultDenumerator, 10);
    const numenatorResInt = parseInt(tasks[taskCounter].resultNumerator, 10);

    // Calc res
    const userRes = (numenatorInt / denumenatorInt).toFixed(3);
    const serverRes = (numenatorResInt / denumenatorResInt).toFixed(3);
    // Correct result
    if (denumenatorInt === denumenatorResInt &&
      numenatorInt === numenatorResInt) {
      // Handle correct response
      setCorrectValueCount(correctValueCount + 1);
    } else {
      //Handle semi right 

      if (userRes === serverRes) {
        setHalfCorrect(true);
      }
      // Handle wrong response
      setShowResult(true);
      setWaiting(true);
    }
  }

  const restartTraining = () => {
    loadData();
    setCorrectValueCount(0);
    setWrongValueCount(0);
  }

  const resetOrderSizeElements = () => {
    setBackgroundColor1('white');
    setBackgroundColor2('white');
    setBackgroundColor3('white');
    setBackgroundColor4('white');

    const json = { numerator: '?', denumerator: '?' };
    setAnswer1(json);
    setAnswer2(json);
    setAnswer3(json);
    setAnswer4(json);

    setOrderTaskIds([]);
  }

  const continueAfterWrongAnswer = () => {
    // Reset backgrounds in order tasks
    if (orderTaskIds.length !== 0) {
      resetOrderSizeElements();
    }

    if (halfCorrect) {
      setHalfCorrect(false);
      setWarningValueCount(warningValueCount + 1);
    } else {
      setWrongValueCount(wrongValueCount + 1);

    }
    setShowResult(false);
    setWaiting(false);
  }

  const fractionClick = (e) => {
    if (waiting) {
      return;
    }
    const answer = e.target.id;
    setWaiting(true);

    if (parseInt(answer, 10) === parseInt(tasks[taskCounter].result, 10)) {
      setCorrectValueCount(correctValueCount + 1);
    } else {
      setShowResult(true);
    }
  }

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      <Paper elevation={0}>
        <Typography variant="h2" align="center">{title}</Typography>

        {/* Loading lecture content*/}

        <Card sx={{ margin: 3, width: 800 }}>
          <CardHeader>
            <Typography variant="h2">Aufgabe</Typography>
          </CardHeader>
          <CardContent>
            {
              lectureComplete ?
                <Box>
                  <Typography variant="h3" align="center">Training beendet!</Typography>
                </Box>
                :

                <form onSubmit={handleSubmit}>
                  <Stack
                    direction="row"
                    spacing={2}
                    divider={<Divider orientation="vertical" flexItem />}
                  >

                    <Grid
                      container
                      spacing={0}
                      alignItems="center"
                      justifyContent="center"
                      width={"20%"}
                    >

                      <Grid item xs={0}>
                        <Typography align="center" variant="h5">Aufgabe</Typography>
                        <Typography align="center" variant="h6">{taskCounter + 1}/{maxTasks}</Typography>
                      </Grid>
                    </Grid>
                    <Box sx={{ width: "100%" }}>
                      <TaskLayout id={id} />
                    </Box>


                    <Grid
                      container
                      spacing={0}
                      alignItems="center"
                      justifyContent="center"
                      width={"20%"}
                    >
                      <Grid item xs={0}>
                        <Stack>
                          {
                            showNextButton && !showResult ?
                              <IconButton sx={{ color: "black" }} size="large" type="submit" disabled={showResult}>
                                <EastRounded />
                              </IconButton>
                              :
                              <Box />
                          }
                          {
                            showResult ?
                              <IconButton sx={{ color: "black" }} size="large" mt={5} onClick={continueAfterWrongAnswer}>
                                <EastRounded />
                              </IconButton>
                              :
                              <Box />
                          }
                        </Stack>
                      </Grid>

                    </Grid>
                  </Stack>


                </form>
            }
            {
              tasks.length > 0 && showHelp && !showResult && !lectureComplete ?
                <Grid
                  container
                  spacing={0}
                  alignItems="center"
                  justifyContent="center"
                  width={"100%"}
                >
                  <Grid item xs={12}>
                    <Fraction />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography align="center" variant="h5">Hilfestellung:</Typography>
                  </Grid>
                  <Grid item xs={8}>
                    <HelperLayout />
                  </Grid>
                </Grid>
                :
                <Box />
            }
            <ResultLayout id={id} />
            {lectureComplete ?
              <Box />
              :
              <Fraction />

            }

            {
              lectureComplete ?
                <Box>
                  <Grid
                    container
                    spacing={2}
                    align="center"
                    alignItems="center"
                    justifyContent="center"
                    sx={{ mt: 0 }}
                  >
                    <Grid item xs={12} >
                      <Typography variant="h5" align="center">Ergebnis:</Typography>

                    </Grid>
                    <Grid item xs={0} >
                      <Icon sx={{ color: 'green' }}>
                        <DoneOutlined />
                      </Icon>
                      <Typography align="center" variant="h6">Richtig: {correctValueCount}</Typography>
                    </Grid>
                    {
                      id !== '2' ?
                        <Grid item xs={0} >
                          <Icon sx={{ color: 'orange' }}>
                            <WarningIcon />
                          </Icon>
                          <Typography align="center" variant="h6">Fast Richtig: {warningValueCount}</Typography>
                        </Grid>
                        :
                        <Box />
                    }
                    <Grid item xs={0} >
                      <Icon sx={{ color: 'red' }}>
                        <Error />
                      </Icon>
                      <Typography align="center" variant="h6">Falsch: {wrongValueCount}</Typography>
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    spacing={2}
                    align="center"
                    alignItems="center"
                    justifyContent="center"
                    sx={{ mt: 0 }}
                  >
                    <Grid item xs={12} >
                      <Button variant="outlined" href="/results" fullWidth>
                        Zur Ergebnisübersicht
                      </Button>
                    </Grid>
                    <Grid item xs={12} >
                      <Button variant="outlined" onClick={restartTraining} fullWidth>
                        Nächstes Training starten
                      </Button>
                    </Grid>
                  </Grid>
                </Box>
                :
                <Grid
                  container
                  spacing={2}
                  align="center"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Grid item xs={0} >
                    <Icon sx={{ color: 'green' }}>
                      <DoneOutlined />
                    </Icon>
                    <Typography align="center" variant="h6">Richtig: {correctValueCount}</Typography>
                  </Grid>
                  {
                    id !== '2' ?
                      <Grid item xs={0} >
                        <Icon sx={{ color: 'orange' }}>
                          <WarningIcon />
                        </Icon>
                        <Typography align="center" variant="h6">Fast Richtig: {warningValueCount}</Typography>
                      </Grid>
                      :
                      <Box />
                  }
                  <Grid item xs={0} >
                    <Icon sx={{ color: 'red' }}>
                      <Error />
                    </Icon>
                    <Typography align="center" variant="h6">Falsch: {wrongValueCount}</Typography>
                  </Grid>
                </Grid>
            }
          </CardContent>
        </Card>
      </Paper>
    </div >
  );
}
