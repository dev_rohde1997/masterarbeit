import React, { useEffect, useState } from "react";
import { Paper, Stack, Typography } from "@mui/material";
import Element from "./Element";

export default function Training() {
  const [userId, setUserId] = useState();
  const [lectures, setLectures] = useState([]);

  useEffect(() => {
    setUserId(sessionStorage.getItem("userId"));
  })
  useEffect(() => {
    if (typeof userId === 'undefined') return;
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/lectures/overview/${userId}`)
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            setLectures(json);
          });
        }
      })
  }, [userId]);
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      <Paper elevation={0}>
        <Typography variant="h2" align="center">Bruchrechnung</Typography>
        <Typography variant="h8" align="center">[Es wird empfohlen die Kurse in chronologischer Reihenfolge zu absolvieren]</Typography>

        <Stack sx={{ marginTop: 2 }} spacing={2}>
          {
            lectures.map((lecture) => (
              <Element
                name={lecture.lectureName}
                progress={lecture.rating}
                id={lecture.lectureId}
                key={lecture.lectureId}
                locked={lecture.locked}>
              </Element>
            ))
          }
        </Stack>
      </Paper>
    </div>
  );
}
