import { Typography, Card, Avatar, Container } from "@mui/material";
import CardContent from "@mui/material/CardContent";
import { ButtonBase, CardHeader, CardActionArea, Icon, Grid } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { grey, indigo, red } from "@mui/material/colors";
import LockIcon from '@mui/icons-material/Lock';
import { Box } from "@mui/system";

const Content = (props) => {
  return (
    <Container>
      {
        props.locked ?
          <CardHeader
            title={props.name}
            subheader={props.level}
            avatar={<LockIcon />}
          ></CardHeader>
          :
          <CardHeader
            title={props.name}
            subheader={props.level}
            avatar={<Avatar sx={{ bgcolor: indigo[500] }}>{props.id}</Avatar>}
          ></CardHeader>
      }
      {
        props.locked ?
          <Box mb={2}/>
          :
          <LinearProgress
            sx={{ height: 10, borderRadius: 5, mb: 1.5, ml: 2, mr: 2 }}
            variant="determinate"
            color="success"
            value={props.progress}
          />
      }
    </Container>
  );
};

export default function Element(props) {
  const [level, setLevel] = useState("");
  const [path, setPath] = useState("" + props.id);

  useEffect(() => {
    if (props.progress < 33) {
      setLevel("Beginner");
    } else if (props.progress < 66) {
      setLevel("Fortgeschritten");
    } else if (props.progress < 100) {
      setLevel("Experte");
    } else {
      setLevel("Kurs abgeschlossen");
    }
  });
  return (
    <Container >
      {
        props.locked ?
          <Card raised={true} sx={{ backgroundColor: "darkgrey" }} >
            <CardActionArea>
              <Content
                id={props.id}
                progress={props.progress}
                level={level}
                name={props.name}
                locked={props.locked}
              ></Content>
            </CardActionArea>
          </Card>
          :
          <Card raised={true}>
            <CardActionArea component={RouterLink} to={path}>
              <Content
                id={props.id}
                progress={props.progress}
                level={level}
                name={props.name}
                locked={props.locked}
              ></Content>
            </CardActionArea>
          </Card>
      }

    </Container>
  );
}
