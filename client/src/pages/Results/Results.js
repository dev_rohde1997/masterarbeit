import React, { useEffect, useState } from "react";
import {
  Typography,
  Paper,
  Button,
  Divider,
  Stack,
  Grid,
  Icon,
  Box,
  Pagination
} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import DoneOutlined from '@mui/icons-material/DoneOutlined';
import WarningIcon from '@mui/icons-material/Warning';

import Error from '@mui/icons-material/Error';

const Headline = () => {
  return (
    <Paper elevation={0}>
      <Stack
        spacing={0}
        direction="row"
        divider={<Divider textAlign="center" sx={{ background: "black" }}
        />}
      >
        <Grid
          container
          spacing={2}
          align="center"
          alignItems="center"
          justifyContent="center"
        >
          <Grid item xs={3} >
            <Typography>Zeit</Typography>
          </Grid>
          <Grid item xs={4} >
            <Typography >Kurstitel</Typography>
          </Grid>
          <Grid item xs={1} >
            <Icon sx={{ color: 'green' }}>
              <DoneOutlined />
            </Icon>
          </Grid>
          <Grid item xs={1} >
            <Icon sx={{ color: 'orange' }}>
              <WarningIcon />
            </Icon>
          </Grid>
          <Grid item xs={1} >
            <Icon sx={{ color: 'red' }}>
              <Error />
            </Icon>
          </Grid>
        </Grid>
      </Stack>
    </Paper >
  );
}
const ResultsEntry = (props) => {

  return (
    <Paper>
      <Stack
        spacing={2}
        direction="row"
        divider={<Divider textAlign="center" sx={{ background: "black" }}
        />}
      >
        <Grid
          container
          spacing={2}
          padding={1}
          align="center"
          alignItems="center"
          justifyContent="center"
        >
          <Grid item xs={4} >
            <Typography>{props.date} {props.time}</Typography>
          </Grid>
          <Grid item xs={3} >
            <Typography >{props.name}</Typography>
          </Grid>
          <Grid item xs={1} >
            <Typography align="center" variant="subtitle1">{props.correct}</Typography>
          </Grid>
          <Grid item xs={1} >
            <Typography align="center" variant="subtitle1">{props.warning}</Typography>
          </Grid>
          <Grid item xs={1} >
            <Typography align="center" variant="subtitle1">{props.wrong}</Typography>
          </Grid>
        </Grid>
      </Stack>
    </Paper >
  );
}
export default function Results() {

  const [userId, setUserId] = useState(0);
  const [results, setResults] = useState([]);
  const [resultsPage, setResultsPage] = useState([]);
  const [pagesTotal, setPagesTotal] = useState(1);
  const [pageNumber, setPageNumber] = useState(1);

  useEffect(() => {
    // Get user
    setUserId(sessionStorage.getItem("userId"));
  });

  useEffect(() => {
    loadData();
  }, [userId])

  useEffect(() => {
    const totalPageNum = results.length / 4;
    setPagesTotal(Math.ceil(totalPageNum));
  }, [results])

  useEffect(() => {
    if (results.length === 0) return;
    setResultsPage(results.slice((pageNumber - 1) * 4, pageNumber * 4));
  }, [pageNumber, results])

  const loadData = () => {
    if (userId === 0) return;
    fetch(`http://${process.env.REACT_APP_API_URI}/api/v1/results/${userId}`,
      { crossDomain: true })
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            setResults(json)
          })
        }
      })
  }

  const paginationChange = (e, newPage) => {
    setPageNumber(newPage);
  }


  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "5em",
      }}
    >
      <Paper elevation={0} sx={{ width: 700 }}>
        <Typography variant="h2" align="center">Ergebnisse</Typography>
        <Card sx={{ mt: 2 }}>
          <CardContent>


            <Stack
              spacing={2}
            >
              <Grid
                container
                spacing={0}
                alignItems="center"
                justifyContent="center"
              >
                <Grid
                  padding={1}
                  item
                >
                  <Pagination
                    count={pagesTotal}
                    page={pageNumber}
                    onChange={paginationChange}
                    variant="outlined"
                    shape="rounded"
                    color="primary" />
                </Grid>
                <Grid xs={12} item>
                  <Headline />
                </Grid>
              </Grid>

              {
                resultsPage.length > 0 ?
                  resultsPage.map((lecture) => (
                    <ResultsEntry
                      time={lecture.time}
                      date={lecture.date}
                      name={lecture.name}
                      correct={lecture.correct}
                      wrong={lecture.wrong}
                      warning={lecture.warning}
                    />
                  ))
                  :
                  <Box>
                    <Typography variant="h5" align="center">
                      Noch keine Ergebnisse
                    </Typography>
                    <Button variant="contained" href="/training" sx={{ mt: 1.5, width: '100%' }}>
                      Training starten
                    </Button>
                  </Box>

              }
            </Stack>
          </CardContent>
        </Card>
      </Paper>
    </div>
  );
}